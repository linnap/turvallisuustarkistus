# Turvallisuustarkistus

## Getting Started
Below you can find how it's done for Android
* Install React Native command line interface `npm install -g react-native-cli`
* Install dependencies `npm install`
* Start TypeScript transpiling `npm run watch`
* Start Android emulator `%ANDROID_HOME%/tools/emulator -avd Nexus_One_API_23`
* Start application in emulator `react-native run-android`
