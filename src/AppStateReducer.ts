import {
    Action,
    ActionTypes,
    ChangeLocaleAction,
    UpdateTargetAction,
    RemoveTargetAction,
    CreateTargetAction,
    SetSafetyCheckAnswerAction,
    AddPhotoToSafetyCheckAction,
    RemovePhotoFromSafetyCheckAction,
    SetContactInfoAction,
    SetRepairReminderFrequencyAction,
    ToggleSafetyReminderAction,
    ToggleSecurityReminderAction,
    SelectTargetAction,
    ClearResultsAction,
    SetSecurityCheckAnswerAction,
    SetSecurityWalkCheckAnswerAction,
    SetWorkplaceSecurityCheckAnswerAction
} from './Actions';
import { initialState, AppState, Target, ReminderFrequency } from './AppState';
import { find, sortBy, max, reduce } from "lodash";
import I18n from 'react-native-i18n';
import { SafetyCheckAnswerTranslations } from "./SafetyCheck/SafetyChecklist";
import { SecurityCheckAnswerTranslations } from "./SecurityCheck/SecurityChecklist";
import { SecurityWalkCheckAnswerTranslations } from "./SecurityWalkCheck/SecurityWalkChecklist";
import { WorkplaceSecurityCheckAnswerTranslations } from "./WorkplaceSecurityCheck/WorkplaceSecurityChecklist";
import * as PushNotification from 'react-native-push-notification';
import { Linking, Platform } from 'react-native';

interface Notification {
    title?: string;
    message: string;
    date?: Date;
    playSound?: boolean; // default: true
    soundName?: 'default' | string;
    repeatType?: 'minute' | 'day' | 'week' | 'month' | 'year' | 'time'
    repeatTime?: number; // If repeat type is 'time'
    actions?: string;
    userInfo: any;
    number?: Number
}

export function appStateReducer(state: AppState = initialState, action: Action) {
    switch (action.type) {
        case ActionTypes.ChangeLocale: {
            const locale = (action as ChangeLocaleAction).locale;
            return Object.assign({}, state, { currentLocale: locale });
        }
        case ActionTypes.RemoveTarget: {
            const targetIdToRemove = (action as RemoveTargetAction).targetId;
            const targets = state.targets.filter(t => t.id !== targetIdToRemove);
            const answers = state.safetyCheckAnswers.filter(a => a.targetId !== targetIdToRemove);
            const photos = state.safetyCheckPhotos.filter(p => p.targetId !== targetIdToRemove);
            return Object.assign({}, state, {
                targets,
                safetyCheckAnswers: answers,
                safetyCheckPhotos: photos
            });
        }
        case ActionTypes.UpdateTarget: {
            const { targetId, name, city, address } = (action as UpdateTargetAction);
            const targetToUpdate = find(state.targets, (t: Target) => t.id === targetId);

            if (targetToUpdate == null) {
                throw new Error(`Failed to remove target with id '${targetId}': target not found!`);
            }

            const targets = state.targets
                .filter(t => t.id !== targetId)
                .concat([{ id: targetId, name, city, address }]);
            const sortedTargets = sortBy(targets, t => t.id);
            return Object.assign({}, state, { targets: sortedTargets });
        }
        case ActionTypes.CreateTarget: {
            const { name, city, address } = action as CreateTargetAction;
            const currentMaxId = max(state.targets.map(t => t.id));
            const targets = state.targets.concat([{ id: (currentMaxId == null ? 1 : currentMaxId + 1), name, city, address }])
            return Object.assign({}, state, { targets });
        }
        case ActionTypes.SetSafetyCheckAnswer: {
            const { targetId, checkItemId, answer } = (action as SetSafetyCheckAnswerAction);
            const currentAnswers = state.safetyCheckAnswers.filter(a => !(a.targetId === targetId && a.checkItemId === checkItemId));
            const answers = currentAnswers.concat({ targetId, checkItemId: checkItemId, answer });
            return Object.assign({}, state, { safetyCheckAnswers: answers });
        }
        case ActionTypes.AddPhotoToSafetyCheck: {
            const { targetId, checkItemId, photoUri } = (action as AddPhotoToSafetyCheckAction);
            const photos = state.safetyCheckPhotos.concat({ targetId, checkItemId, photoUri })
            return Object.assign({}, state, { safetyCheckPhotos: photos });
        }
        case ActionTypes.RemovePhotoFromSafetyCheck: {
            const { targetId, checkItemId, photoUri } = (action as RemovePhotoFromSafetyCheckAction);
            const photos = state.safetyCheckPhotos.filter(p => !(p.targetId === targetId && p.checkItemId === checkItemId && p.photoUri === photoUri));
            return Object.assign({}, state, { safetyCheckPhotos: photos });
        }
        case ActionTypes.SetSecurityCheckAnswer: {
            const { targetId, checkItemId, answer } = (action as SetSecurityCheckAnswerAction);
            const currentAnswers = state.securityCheckAnswers.filter(a => !(a.targetId === targetId && a.checkItemId === checkItemId));
            const answers = currentAnswers.concat({ targetId, checkItemId: checkItemId, answer });
            return Object.assign({}, state, { securityCheckAnswers: answers });
        }
        case ActionTypes.SetSecurityWalkCheckAnswer: {
            const { targetId, checkItemId, answer } = (action as SetSecurityWalkCheckAnswerAction);
            const currentAnswers = state.securityWalkCheckAnswers.filter(a => !(a.targetId === targetId && a.checkItemId === checkItemId));
            const answers = currentAnswers.concat({ targetId, checkItemId: checkItemId, answer });
            return Object.assign({}, state, { securityWalkCheckAnswers: answers });
        }
        case ActionTypes.SetWorkplaceSecurityCheckAnswer: {
            const { targetId, checkItemId, answer } = (action as SetWorkplaceSecurityCheckAnswerAction);
            const currentAnswers = state.workplaceSecurityCheckAnswers.filter(a => !(a.targetId === targetId && a.checkItemId === checkItemId));
            const answers = currentAnswers.concat({ targetId, checkItemId: checkItemId, answer });
            return Object.assign({}, state, { workplaceSecurityCheckAnswers: answers });
        }
        case ActionTypes.SetContactInfo: {
            const contactInfo = (action as SetContactInfoAction).contactInfo;
            return Object.assign({}, state, { contactInfo });
        }
        case ActionTypes.SetRepairReminderFrequency: {
            PushNotification.cancelAllLocalNotifications();

            const frequency = (action as SetRepairReminderFrequencyAction).frequency;
            enableRepairReminder(frequency);

            if (state.securityReminderEnabled) {
                enableSecurityReminder(state.securityReminderEnabledTimestamp);
            }

            if (state.safetyReminderEnabled) {
                enableSafetyReminder(state.safetyReminderEnabledTimestamp);
            }

            return Object.assign({}, state, { repairReminderFrequency: frequency });
        }
        case ActionTypes.ToggleSafetyReminder: {
            const isEnabled = (action as ToggleSafetyReminderAction).isEnabled;
            if (isEnabled) {
                enableSafetyReminder(state.safetyReminderEnabledTimestamp);
            } else {
                PushNotification.cancelAllLocalNotifications();
                enableRepairReminder(state.repairReminderFrequency);
                if (state.securityReminderEnabled) {
                    enableSecurityReminder(state.securityReminderEnabledTimestamp);
                }
            }
            return Object.assign({}, state, {
                safetyReminderEnabled: isEnabled,
                safetyReminderEnabledTimestamp: isEnabled ? new Date(Date.now()) : null
            });
        }
        case ActionTypes.ToggleSecurityReminder: {
            const isEnabled = (action as ToggleSecurityReminderAction).isEnabled;
            if (isEnabled) {
                enableSecurityReminder(state.securityReminderEnabledTimestamp);
            } else {
                PushNotification.cancelAllLocalNotifications();
                enableRepairReminder(state.repairReminderFrequency);
                if (state.safetyReminderEnabled) {
                    enableSafetyReminder(state.safetyReminderEnabledTimestamp);
                }
            }
            return Object.assign({}, state, {
                securityReminderEnabled: isEnabled,
                securityReminderEnabledTimestamp: isEnabled ? new Date(Date.now()) : null
            });
        }
        case ActionTypes.SelectTarget: {
            const target = (action as SelectTargetAction).target;
            return Object.assign({}, state, { selectedTarget: target });
        }
        case ActionTypes.ClearResults: {
            const targetId = (action as ClearResultsAction).targetId;
            const safetyAnswers = state.safetyCheckAnswers.filter(a => a.targetId !== targetId);
            const securityAnswers = state.securityCheckAnswers.filter(a => a.targetId !== targetId);
            const securityWalkAnswers = state.securityWalkCheckAnswers.filter(a => a.targetId !== targetId);
            const workplaceSecurityCheckAnswers = state.workplaceSecurityCheckAnswers.filter(a => a.targetId !== targetId);
            const photos = state.safetyCheckPhotos.filter(p => p.targetId !== targetId);
            return Object.assign({}, state, {
                safetyCheckAnswers: safetyAnswers,
                securityCheckAnswers: securityAnswers,
                securityWalkCheckAnswers: securityWalkAnswers,
                workplaceSecurityCheckAnswers: workplaceSecurityCheckAnswers,
                safetyCheckPhotos: photos
            })
        }
        case ActionTypes.SendPublicResults: {
            const mailContent = reduce(state.targets, (content, target) => {
                const resultStr = reduce(state.safetyChecklist, (result, check) => {
                    const safetyAnswer = find(state.safetyCheckAnswers, a => a.targetId === target.id && a.checkItemId === check.id);
                    return `${result}\n\n${check.text}\n--> ${safetyAnswer ? I18n.t(SafetyCheckAnswerTranslations[safetyAnswer.answer]) : I18n.t('common.noAnswer')}`;
                }, I18n.t('safety.title'));

                const title = [target.name, target.address, target.city]
                    .filter(part => part != null && part.length > 0)
                    .join(', ')
                return `${content}\n\n*** ${title} ***\n\n${resultStr}\n\n`;
            }, I18n.t('common.results'));

            Linking.openURL(`mailto:omavalvonta@satapelastus.fi?subject=Turvallisuustarkistus&body=${mailContent}`);
			return state;
        }
        case ActionTypes.SendPrivateResults: {
            const mailContent = reduce(state.targets, (content, target) => {
                const safetyResultStr = reduce(state.safetyChecklist, (result, check) => {
                    const safetyAnswer = find(state.safetyCheckAnswers, a => a.targetId === target.id && a.checkItemId === check.id);
                    return `${result}\n\n${check.text}\n--> ${safetyAnswer ? I18n.t(SafetyCheckAnswerTranslations[safetyAnswer.answer]) : I18n.t('common.noAnswer')}`;
                }, I18n.t('safety.title'));

                const securityResultStr = reduce(state.securityChecklist, (result, check) => {
                    const securityAnswer = find(state.securityCheckAnswers, a => a.targetId === target.id && a.checkItemId === check.id);
                    return `${result}\n\n${check.text}\n--> ${securityAnswer ? I18n.t(SecurityCheckAnswerTranslations[securityAnswer.answer]) : I18n.t('common.noAnswer')}`;
                }, I18n.t('security.title'));

                const securityWalkResultStr = reduce(state.securityWalkChecklist, (result, check) => {
                    const securityWalkAnswer = find(state.securityWalkCheckAnswers, a => a.targetId === target.id && a.checkItemId === check.id);
                    return `${result}\n\n${check.text}\n--> ${securityWalkAnswer ? I18n.t(SecurityWalkCheckAnswerTranslations[securityWalkAnswer.answer]) : I18n.t('common.noAnswer')}`;
                }, I18n.t('securityWalk.title'));

                const workplaceSecurityResultStr = reduce(state.workplaceSecurityChecklist, (result, check) => {
                    const workplaceSecurityAnswer = find(state.workplaceSecurityCheckAnswers, a => a.targetId === target.id && a.checkItemId === check.id);
                    return `${result}\n\n${check.text}\n--> ${workplaceSecurityAnswer ? I18n.t(WorkplaceSecurityCheckAnswerTranslations[workplaceSecurityAnswer.answer]) : I18n.t('common.noAnswer')}`;
                }, I18n.t('workplaceSecurityCheck.title'));

                const title = [target.name, target.address, target.city]
                    .filter(part => part != null && part.length > 0)
                    .join(', ')
                return `${content}\n\n*** ${title} ***\n\n${safetyResultStr}\n\n${securityResultStr}\n\n${securityWalkResultStr}\n\n${workplaceSecurityResultStr}\n\n`;
            }, I18n.t('common.results'));

            Linking.openURL(`mailto:?subject=Tarkistustulokset&body=${mailContent}`);
			return state;
        }
        default:
            return state;
    }
}

function enableRepairReminder(frequency: ReminderFrequency) {
    const notification: Notification = {
        title: 'Turvallisuustarkistus',
        message: I18n.t('notifications.repairReminderMessage'),
        userInfo: { id: 'TurvallisuustarkistusRepairReminder' }
    };

    if (Platform.OS === 'ios') { notification.number = 0 };

    switch (frequency) {
        case ReminderFrequency.Off: {
            break;
        }
        case ReminderFrequency.Daily: {
            notification.repeatType = 'day';
            notification.date = new Date(Date.now() + 60 * 1000 * 60 * 24);
            PushNotification.localNotificationSchedule(notification);
            break;
        }
        case ReminderFrequency.Weekly: {
            notification.repeatType = 'week';
            notification.date = new Date(Date.now() + 60 * 1000 * 60 * 24 * 7);
            PushNotification.localNotificationSchedule(notification);
            break;
        }
        case ReminderFrequency.Monthly: {
            const oneMonthInMillis = 60 * 1000 * 60 * 24 * 30;
            if (Platform.OS === 'ios') {
                notification.repeatType = 'month';
            } else {
                notification.repeatType = 'time';
                notification.repeatTime = oneMonthInMillis;
            }
            notification.date = new Date(Date.now() + oneMonthInMillis);
            PushNotification.localNotificationSchedule(notification);
            break;
        }
    }
}

function enableSafetyReminder(enabledTimestamp?: Date | null) {
    const oneYearInMillis = 60 * 1000 * 60 * 24 * 365;
    const start = new Date((enabledTimestamp == null ? Date.now() : enabledTimestamp.getTime()) + oneYearInMillis);
    const notification: Notification = {
        title: 'Turvallisuustarkistus',
        message: I18n.t('notifications.safetyReminderMessage'),
        userInfo: { id: 'TurvallisuustarkistusSafetyReminder' },
        date: start
    };
    if (Platform.OS === 'ios') {
        notification.repeatType = 'year';
        notification.number = 0;
    } else {
        notification.repeatType = 'time';
        notification.repeatTime = oneYearInMillis;
    }

    PushNotification.localNotificationSchedule(notification);
}

function enableSecurityReminder(enabledTimestamp?: Date | null) {
    const oneYearInMillis = 60 * 1000 * 60 * 24 * 365;
    const start = new Date((enabledTimestamp == null ? Date.now() : enabledTimestamp.getTime()) + oneYearInMillis);
    const notification: Notification = {
        title: 'Turvallisuustarkistus',
        message: I18n.t('notifications.securityReminderMessage'),
        userInfo: { id: 'TurvallisuustarkistusSecurityReminder' },
        date: start
    };
    if (Platform.OS === 'ios') {
        notification.repeatType = 'year';
        notification.number = 0;
    } else {
        notification.repeatType = 'time';
        notification.repeatTime = oneYearInMillis;
    }
    PushNotification.localNotificationSchedule(notification);
}

