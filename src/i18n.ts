import I18n from 'react-native-i18n';

// Applicationwide settings
I18n.locale = 'fi';
I18n.defaultLocalt = 'fi';

// Translations
I18n.translations = {
    fi: {
        common: {
            back: 'Takaisin',
            cancel: 'Peruuta',
            remove: 'Poista',
			noAnswer: 'Ei vastausta',
            noTargetsDefined: 'Et ole määritellyt yhtään kohdetta. Kohteita voi määritellä Asetukset-sivulla.',
            addPicture: 'Lisää kuva',
            sendPrivateResults: 'Lähetä tulokset omaan sähköpostiin',
            sendPublicResults: 'Lähetä Kodin omavalvonnan tulokset viranomaiselle',
            checkDone: 'Kysely on päättynyt',
            takePhotoButtonTitle: 'Ota kuva...',
            chooseFromLibraryButtonTitle: 'Valitse kirjastosta...',
			results: 'Tulokset',
            frequency: {
                off: 'Ei muistutuksia',
                daily: 'Päivittäin',
                weekly: 'Viikottain',
                monthly: 'Kuukausittain'
            }
        },
        home: {
            title: 'Aloitussivu',
            nav: {
                safety: 'Kodin omavalvonta',
                security: 'Tietoturva',
                securityWalk: 'Turvallisuuskävely',
                workplaceSecurityCheck: 'Työpaikan turvallisuustarkistus',
                results: 'Tulokset',
                settings: 'Asetukset'
            }
        },
        safety: {
            title: 'Kodin omavalvonta',
            answerOk: 'Kunnossa',
            answerNotOk: 'Korjattava',
            answerNoAction: 'Ei toimenpiteitä'
        },
        security: {
            title: 'Tietoturva',
            answerOk: 'Kunnossa',
            answerNotOk: 'Korjattava',
            answerNoAction: 'Ei toimenpiteitä'
        },
        securityWalk: {
            title: 'Turvallisuuskävely',
            answerOk: 'Kunnossa',
            answerNotOk: 'Korjattava',
            answerNoAction: 'Ei toimenpiteitä'
        },
        workplaceSecurityCheck: {
            title: 'Työpaikan turvallisuustarkistus',
            answerOk: 'Kunnossa',
            answerNotOk: 'Korjattava',
            answerNoAction: 'Ei toimenpiteitä'
        },
        settings: {
            title: 'Asetukset',
            reminderSectionTitle: 'Muistutukset',
            languagesSectionTitle: 'Kieli',
            targetsSectionTitle: 'Kohteet',
            contactSectionTitle: 'Yhteystiedot',
            contactFieldNameTitle: 'Nimi',
            contactFieldAddressTitle: 'Osoite',
            contactFieldZipCodeTitle: 'Postinumero',
            contactFieldCityTitle: 'Kunta',
            contactFieldPhoneNumberTitle: 'Puhelinnumero',
            saveContactInfo: 'Tallenna yhteystiedot',
            addTarget: 'Lisää kohde',
            saveNewTarget: 'Tallenna uusi kohde',
            remindToRepair: 'Muistuta korjattavista kohteista',
            sendYearlyReminder: 'Anna tarkistuksen vuosimuistutus'
        },
        target: {
            nameFieldTitle: 'Kohteen nimi',
            addressFieldTitle: 'Osoite',
            cityFieldTitle: 'Kunta/kaupunki',
            remove: 'Poista kohde',
            saveName: 'Tallenna tiedot',
            clearResults: 'Nollaa kohteen vastaukset'
        },
        targetSelection: {
            selectTarget: 'Valitse kohde'
        },
        results: {
            title: 'Tulokset',
            selectTarget: 'Kohde',
            thingsToFix: 'Korjattavat asiat',
            safety: 'Kodin omavalvonta',
            security: 'Tietoturva',
            securityWalk: 'Turvallisuuskävely',
            workplaceSecurityCheck: 'Työpaikan turvallisuustarkistus',
            noFixables: 'Ei korjattavia asioita'
        },
        info: {
            title: 'Tietoja sovelluksesta',
            descriptionFirstParagraph: 'Turvallisuustarkistus-mobiilisovellus on SATTUKO-hankkeessa kehitetty työkalu, jonka avulla jokainen ihminen voi itse tehdä omaan kotiinsa liittyviä turvallisuustarkastuksia helposti.',
            descriptionSecondParagraph: 'Mobiilisovelluksen kyselyt ovat Omavalvonta (kodin paloturvallisuus) ja Tietoturvallisuus (kodin tietoturvallisuus). Omavalvonta–kysely on laadittu yhdessä Satakunnan pelastuslaitoksen kanssa. Tietoturva–kysely on laadittu Tampereen teknillisen yliopiston Porin yksikössä.'
        }, /* TODO: description for turvallisuuskävely? */
        notifications: {
            repairReminderMessage: 'Muista korjata turvallisuustarkistuksessa havaitut puutteet!',
            safetyReminderMessage: 'Kodin omavalvonta: vuositarkistuksen aika!',
            securityReminderMessage: 'Tietoturva: vuositarkistuksen aika!'
            /* TODO SecurityWalkCheck and WorkplaceSecurityCheck reminders ? */
        }
    },
    sv: {
        home: {
            title: 'Säkerhet översyn',
            nav: {
                safety: 'Trygghet',
                security: 'Säkerhet',
                securityWalk: 'Turvallisuuskävely', /* TODO in Swedish ?*/
                workplaceSecurityCheck: 'Työpaikan turvallisuustarkistus', /* TODO in Swedish ?*/
                results: 'Utfall'
            }
        },
        settings: {
            title: 'Inställningar',
            languagesTitle: 'Språk'
        }
    },
    en: {
        home: {
            title: 'Safety Check',
            nav: {
                safety: 'Safety',
                security: 'Security',
                securityWalk: 'Turvallisuuskävely', /* TODO in English ?*/
                workplaceSecurityCheck: 'Työpaikan turvallisuustarkistus', /* TODO in English ?*/
                results: 'Results'
            }
        },
        settings: {
            title: 'Settings',
            languagesTitle: 'Language',
        }

    }
}
