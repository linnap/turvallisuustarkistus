import { sortBy } from 'lodash';

export interface SafetyCheckItem {
    id: number;
    sortIndex: number;
    text: string;
    infoText?: string;
    linkToApp?: LinkToApp
}

export interface LinkToApp {
    appStoreId: string;
    playStoreId: string;
    appIcon: any;
}

export interface SafetyCheckAnswer {
    targetId: number;
    checkItemId: number;
    answer: SafetyCheckAnswerType;
}


export enum SafetyCheckAnswerType {
    Ok = 'OK', NotOk = 'NOK', NoAction = 'NO_ACTION'
}

export const SafetyCheckAnswerTranslations: { [answer: number]: string } = {
    [SafetyCheckAnswerType.NotOk]: 'safety.answerNotOk',
    [SafetyCheckAnswerType.Ok]: 'safety.answerOk',
    [SafetyCheckAnswerType.NoAction]: 'safety.answerNoAction',
}

export interface SafetyCheckPhoto {
    targetId: number;
    checkItemId: number;
    photoUri: string;
}

export const initialSafetyChecklist: Array<SafetyCheckItem> = sortBy([
    {
        id: 0,
        sortIndex: 0,
        text: 'Tunnen hätänumeron 112 ja osaan tehdä hätäilmoituksen',
        // infoText: '',
        linkToApp: { appStoreId: 'id998281396', playStoreId: 'fi.digia.suomi112', appIcon: require('../../images/112.png') }
    },
    {
        id: 1,
        sortIndex: 1,
        text: 'Olen tehnyt kotiini pelastautumissuunnitelman ja varautunut häiriötilanteisiin'
    },
    {
        id: 2,
        sortIndex: 2,
        text: 'Palovaroittimien testaus, kunto ja määrä (1 palovaroitin / kerroksen alkava 60m2)'
    },
    {
        id: 3,
        sortIndex: 3,
        text: 'Asunnossa on alkusammutusvälineet ja osaan käyttää niitä'
    },
    {
        id: 4,
        sortIndex: 4,
        text: 'Toisen kerroksen varatiepoistuminen voidaan suorittaa turvallisesti ja varateiden ikkunoihin on asennettu kiinteät kahvat'
    },
    {
        id: 5,
        sortIndex: 5,
        text: 'Turvalaitteet katolla on kunnossa ja huollettu'
    },
    {
        id: 6,
        sortIndex: 6,
        text: 'Autosuoja on siihen tarkoitetussa käytössä ja sinne ei ole säilötty sinne kuulumatonta materiaalia'
    },
    {
        id: 7,
        sortIndex: 7,
        text: 'Kemikaaleja säilytetään määräysten mukaisesti (mm. bensa, nestekaasu tai tärpätti)'
    },
    {
        id: 8,
        sortIndex: 8,
        text: 'Rakennuksen (osoite- ja) numerokyltti on kunnossa ja näkyy tielle'
    },
    {
        id: 9,
        sortIndex: 9,
        text: 'Tulisijat ja hormit on nuohottu säännöllisesti nuohoojan toimesta (vuosittain)'
    },
    {
        id: 10,
        sortIndex: 10,
        text: 'Kotitapaturman riskit on pyritty minimoimaan. Helposti syttyvää materiaalia ei säilytetä rakennuksen läheisyydessä. Piha-alue on valaistu ja talvella hiekoitettu.'
    },
    {
        id: 11,
        sortIndex: 11,
        text: 'Sähkölaitteiden käyttöturvallisuus on huomioitu'
    },
    {
        id: 12,
        sortIndex: 12,
        text: 'Saunan tekniset ratkaisut ovat paloturvallisia ja käyttö on turvallista'
    },
    {
        id: 13,
        sortIndex: 13,
        text: 'Kodin lääkkeitä säilytetään lukitussa kaapissa niin, että ne eivät ole lasten ulottuvissa'
    },
], (item) => item.sortIndex);
