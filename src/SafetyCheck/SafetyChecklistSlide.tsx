import * as React from "react";
import { View, StyleSheet, Text, TouchableOpacity, ScrollView, Image, Modal } from "react-native";
import { Theme } from '../theme';
import Icon from 'react-native-vector-icons/FontAwesome';
import I18n from 'react-native-i18n';
import { SafetyCheckAnswerType, SafetyCheckAnswerTranslations, LinkToApp } from './SafetyChecklist';
import { IconButton } from '../Components/IconButton';
import { fill } from 'lodash';
import { openInStore } from 'react-native-app-link';
const ImagePicker = require("react-native-image-picker");

interface Props {
    index: number;
    text: string;
    infoText?: string;
    linkToApp?: LinkToApp;
    answer?: SafetyCheckAnswerType;
    photoUris: string[];
    onAnswerClick: (answer: SafetyCheckAnswerType) => void;
    onPhotoAdded: (uri: string) => void;
    onPhotoRemoved: (uri: string) => void;
}

interface State {
    isPhotoViewVisible: boolean;
    selectedPhotoUri: string;
}

export class SafetyChecklistSlide extends React.Component<Props, State>{

    constructor(props: Props) {
        super(props);
        this.state = { isPhotoViewVisible: false, selectedPhotoUri: '' };
    }

    closePhotoModal() {
        this.setState({ isPhotoViewVisible: false, selectedPhotoUri: '' });
    }

    removePhotoAndCloseModal() {
        this.props.onPhotoRemoved(this.state.selectedPhotoUri);
        this.setState({ isPhotoViewVisible: false, selectedPhotoUri: '' });
    }

    render() {
        var options = {
            title: I18n.t('common.addPicture'),
            takePhotoButtonTitle: I18n.t('common.takePhotoButtonTitle'),
            chooseFromLibraryButtonTitle: I18n.t('common.chooseFromLibraryButtonTitle'),
            storageOptions: {
                skipBackup: true,
                path: 'turvallisuustarkistus'
            }
        };

        const showImagePicker = () => {
            ImagePicker.showImagePicker(options, (response) => {
                if (response.didCancel) {
                    // Do nothing
                }
                else if (response.error) {
                    alert(response.error);
                }
                else if (response.customButton) {
                    // No custom buttons
                }
                else {
                    this.props.onPhotoAdded(response.uri)
                }
            });
        }

        return (
            <ScrollView contentContainerStyle={styles.slide}>
                <View style={styles.textSection}>
                    <Text style={styles.text}>
                        {`${this.props.index}. ${this.props.text}`}
                    </Text>
                </View>
                <View>
                    <Text>{this.props.infoText}</Text>
                </View>

                {this.props.linkToApp ?
                    (<View style={{ paddingTop: 10 }}>
                        <TouchableOpacity style={{ width: 64 }} onPress={() => openInStore(this.props.linkToApp!!.appStoreId, this.props.linkToApp!!.playStoreId).catch(err => alert(err))}>
                            <Image
                                style={styles.appIcon}
                                source={this.props.linkToApp.appIcon} />
                        </TouchableOpacity>
                    </View>) : null}

                <View style={styles.answerSection}>
                    {
                        Object.keys(SafetyCheckAnswerTranslations).map(answer => (
                            <TouchableOpacity
                                key={answer}
                                style={styles.answer}
                                onPress={() => this.props.onAnswerClick(answer as SafetyCheckAnswerType)}>
                                <Text>
                                    {I18n.t(SafetyCheckAnswerTranslations[answer])}
                                </Text>
                                <Icon
                                    name={this.props.answer != null && this.props.answer === answer ? "check-square-o" : "square-o"}
                                    color={Theme.gray}
                                    size={38} />
                            </TouchableOpacity>
                        ))
                    }
                </View>
                <View style={styles.photoSection}>
                    <View style={styles.photoList}>
                        {/* Photo  */}
                        {this.props.photoUris.map((uri, index) => (
                            <TouchableOpacity
                                key={index}
                                style={styles.photoListItem}
                                onPress={() => this.setState({ isPhotoViewVisible: true, selectedPhotoUri: uri })}>
                                <Image
                                    style={{
                                        height: 60,
                                        width: 60,
                                        borderRadius: 10,
                                        borderWidth: 1,
                                        borderColor: Theme.gray,
                                    }}
                                    source={{ uri: uri }} />
                            </TouchableOpacity>
                        ))}
                        {/* Placeholders  */}
                        {
                            fill(Array(4 - this.props.photoUris.length), '').map((_, index) => (
                                <View
                                    key={index}
                                    style={styles.photoListItemPlaceholder}>
                                    <Image
                                        style={{ height: 48, width: 48 }}
                                        source={require('../../images/photo_placeholder.png')} />
                                </View>
                            ))
                        }
                    </View>
                    <IconButton
                        color={Theme.gray}
                        disabled={this.props.photoUris.length > 3}
                        icon="camera"
                        text={I18n.t('common.addPicture')}
                        onPress={() => showImagePicker()} />
                </View>
                {/* PHOTO MODAL  */}
                <Modal
                    animationType={'slide'}
                    transparent={false}
                    visible={this.state.isPhotoViewVisible}
                    onRequestClose={() => this.closePhotoModal()}>
                    <View style={styles.photoModal}>
                        <Image
                            style={{ flex: 1, paddingBottom: Theme.screenMargin }}
                            resizeMode="contain"
                            source={{ uri: this.state.selectedPhotoUri }} />
                        <View style={{ paddingBottom: Theme.screenMargin }}>
                            <IconButton
                                icon="trash"
                                color={Theme.red}
                                disabled={false}
                                text={I18n.t('common.remove')}
                                onPress={() => this.removePhotoAndCloseModal()} />
                        </View>
                        <IconButton
                            icon="arrow-left"
                            color={Theme.gray}
                            disabled={false}
                            text={I18n.t('common.back')}
                            onPress={() => this.closePhotoModal()} />
                    </View>
                </Modal>
            </ScrollView>);
    }
}

const styles = StyleSheet.create({
    slide: {
        padding: Theme.screenMargin,
        backgroundColor: Theme.white,
    },
    textSection: {
        paddingBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: Theme.gray,
        fontSize: 20,
        fontWeight: 'bold',
    },
    answerSection: {
        flex: 1,
        flexDirection: 'row',
        alignSelf: 'stretch',
        justifyContent: 'space-between',
        paddingTop: 20,
        paddingBottom: 20
    },
    answer: {
        flex: 1,
        alignItems: 'center',
    },
    photoSection: {
        flex: 1
    },
    photoList: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingBottom: 10
    },
    photoListItem: {
        margin: 5,
    },
    photoListItemPlaceholder: {
        margin: 5,
        padding: 5,
        borderWidth: 1,
        borderColor: Theme.gray,
        borderRadius: 10,
        borderStyle: 'dashed',
        backgroundColor: Theme.lightGray
    },
    photoModal: {
        flex: 1,
        padding: Theme.screenMargin
    },
    appIcon: {
        borderRadius: 10
    }
})
