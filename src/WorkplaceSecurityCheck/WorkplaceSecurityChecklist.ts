import { sortBy } from 'lodash';

export interface WorkplaceSecurityCheckItem {
    id: number;
    sortIndex: number;
    text: string;
    infoText?: string;
}


export interface WorkplaceSecurityCheckAnswer {
    targetId: number;
    checkItemId: number;
    answer: WorkplaceSecurityCheckAnswerType;
}

export enum WorkplaceSecurityCheckAnswerType {
    Ok = 'OK', NotOk = 'NOK', NoAction = 'NO_ACTION'
}

export const WorkplaceSecurityCheckAnswerTranslations: { [answer: number]: string } = {
    [WorkplaceSecurityCheckAnswerType.NotOk]: 'workplaceSecurityCheck.answerNotOk',
    [WorkplaceSecurityCheckAnswerType.Ok]: 'workplaceSecurityCheck.answerOk',
    [WorkplaceSecurityCheckAnswerType.NoAction]: 'workplaceSecurityCheck.answerNoAction',
}

export const initialWorkplaceSecurityChecklist: Array<WorkplaceSecurityCheckItem> = sortBy([
    {
        id: 0,
        sortIndex: 0,
        text: 'Pelastussuunnitelma on tehty ja päivitetty'
    },
    {
        id: 1,
        sortIndex: 1,
        text: 'Lakisääteinen poistumisturvallisuusselvitys on tehty ja on pelastussuunnitelman liitteenä'
    },
    {
        id: 2,
        sortIndex: 2,
        text: 'Henkilökunta on koulutettu turvallisuusasioihin'
    },
    {
        id: 3,
        sortIndex: 3,
        text: 'Työpaikan riskienkartoitus ja vaarojen arviointi tehdään säännöllisesti'
    },
    {
        id: 4,
        sortIndex: 4,
        text: 'Työntekijät osallistuvat riskien kartoitukseen ja vaarojen arviointiin'
    },
    {
        id: 5,
        sortIndex: 5,
        text: 'Uudet työntekijät ja sijaiset perehdytetään turvallisuusasioihin aina'
    },
    {
        id: 6,
        sortIndex: 6,
        text: 'Opiskelijat perehdytetään aina myös turvallisuusasioihin'
    },
    {
        id: 7,
        sortIndex: 7,
        text: 'Tietoturva-asiat ja salassapitovelvollisuus käydään läpi jokaisen uuden työntekijän ja opiskelijan kanssa'
    },
    {
        id: 8,
        sortIndex: 8,
        text: 'Toimialakohtaiset erityissuunnitelmat ovat tehty ja käytössä (esim. sote-alalla lääkehoitosuunnitelma)'
    },
    {
        id: 9,
        sortIndex: 9,
        text: 'Jätehuoltoasiat ja lajitteluohjeet ovat hyvin henkilökunnan tiedossa'
    },
    {
        id: 10,
        sortIndex: 10,
        text: 'Vaaralliset aineet, kemikaalit ja riskijäte on varastoitu ohjeiden mukaisesti'
    },
    {
        id: 11,
        sortIndex: 11,
        text: 'Ensiapuvälineet ovat kaikkien tiedossa, tarkastettu ja merkittynä löytyvät helposti'
    },
    {
        id: 12,
        sortIndex: 12,
        text: 'Työpaikalla seurataan säännöllisesti poikkeamia, vaaratapahtumia, uhkatilanteita ja läheltä piti -tilanteita'
    },
    {
        id: 13,
        sortIndex: 13,
        text: 'Jokainen työntekijä tietää, miten eri vaaratilanteet ja läheltä piti –tilanteet ilmoitetaan eteenpäin tiedoksi'
    },
    {
        id: 14,
        sortIndex: 14,
        text: 'Työpaikalla on varauduttu uhka- ja väkivaltatilanteisiin'
    },
    {
        id: 15,
        sortIndex: 15,
        text: 'Ohjeet haastavan asiakkaan kohtaamisesta ovat kaikkien tiedossa'
    },
    {
        id: 16,
        sortIndex: 16,
        text: 'Toimintaohjeet eri hätätilanteisiin on tehty ja helposti saatavilla'
    },
    {
        id: 17,
        sortIndex: 17,
        text: 'Toimintaohjeet hätätilanteissa ohjeistetaan uusille työntekijöille ja opiskelijoille'
    },
    {
        id: 18,
        sortIndex: 18,
        text: 'Tulipalotilanteen toimintaohjeet ja poistumiskartat ovat esillä mm. ilmoitustauluilla '
    },
    {
        id: 19,
        sortIndex: 19,
        text: 'Hätäpoistumistiet on merkattu selvästi ja merkinnät näkyvät myös pimeässä'
    },
    {
        id: 20,
        sortIndex: 20,
        text: 'Hätäpoistumisteiden ovet ovat avattavissa ja aukeavat ilman avainta'
    },
    {
        id: 21,
        sortIndex: 21,
        text: 'Jos ovet ovat lukossa, lukitusohjeet ja avaaminen hätätilanteessa on ohjeistettu henkilöstölle'
    },
    {
        id: 22,
        sortIndex: 22,
        text: 'Hätäpoistumisteillä ja poistumisreiteillä ei säilytetä tavaraa'
    },
    {
        id: 23,
        sortIndex: 23,
        text: 'Alkusammutusvälineet (sammutin, pikapaloposti, sammutuspeite) ovat helposti käyttöönotettavissa'
    },
    {
        id: 24,
        sortIndex: 24,
        text: 'Alkusammutusvälineet on merkattu, tarkastettu ja huollettu säännöllisesti'
    },
    {
        id: 25,
        sortIndex: 25,
        text: 'Tulipalotilanteessa henkilökunta osaa käyttää alkusammutusvälineitä'
    },
    {
        id: 26,
        sortIndex: 26,
        text: 'Työpaikalla on palo-osastointi ja palo-ovet pidetään kiinni savun leviämisen estämiseksi'
    },
    {
        id: 27,
        sortIndex: 27,
        text: 'Työpaikalla on automaattinen palonilmaisinjärjestelmä'
    },
    {
        id: 28,
        sortIndex: 28,
        text: 'Työpaikalla on sprinklerijärjestelmä (automaattinen sammutuslaitteisto)'
    },
    {
        id: 29,
        sortIndex: 29,
        text: 'Kokoontumispaikka on sovittu ja se on kaikkien tiedossa'
    },
    {
        id: 30,
        sortIndex: 30,
        text: 'Piha-alueen pysäköinti on ohjeistettu ja pelastustiet pysyvät vapaina'
    },
    {
        id: 31,
        sortIndex: 31,
        text: 'Piha-alueen ja kulkureittien valaistus on riittävää'
    },
    {
        id: 32,
        sortIndex: 32,
        text: 'Jätehuolto ja roskakatokset ovat riittävän etäällä rakennuksista'
    },
    {
        id: 33,
        sortIndex: 33,
        text: 'Portaat, käytävät ja kulkuväylät ovat puhtaat ja liukkaus on estetty'
    },
    {
        id: 34,
        sortIndex: 34,
        text: 'Sovittu yhteinen kokoontumispaikka löytyy helposti ja on tarpeeksi etäällä tapahtumapaikasta'
    },
    {
        id: 35,
        sortIndex: 35,
        text: 'Työpaikka on turvallinen asiakkaille, työntekijöille ja vierailijoille'
    }], (item) => item.sortIndex);

