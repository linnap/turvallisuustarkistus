import * as React from "react";
import { View, StyleSheet, Text, TouchableOpacity, ScrollView } from "react-native";
import { Theme } from '../theme';
import Icon from 'react-native-vector-icons/FontAwesome';
import I18n from 'react-native-i18n';
import { WorkplaceSecurityCheckAnswerTranslations, WorkplaceSecurityCheckAnswerType } from './WorkplaceSecurityChecklist';

interface Props {
    index: number;
    text: string;
    infoText?: string;
    answer?: WorkplaceSecurityCheckAnswerType;
    onAnswerClick: (answer: WorkplaceSecurityCheckAnswerType) => void;
}

export class WorkplaceSecurityChecklistSlide extends React.Component<Props, {}>{
    render() {
        return (
            <ScrollView contentContainerStyle={styles.slide}>
                <View style={styles.textSection}>
                    <Text style={styles.text}>
                        {`${this.props.index}. ${this.props.text}`}
                    </Text>
                </View>
                <View>
                    <Text>{this.props.infoText}</Text>
                </View>

                <View style={styles.answerSection}>
                    {
                        Object.keys(WorkplaceSecurityCheckAnswerTranslations).map(answer => (
                            <TouchableOpacity
                                key={answer}
                                style={styles.answer}
                                onPress={() => this.props.onAnswerClick(answer as WorkplaceSecurityCheckAnswerType)}>
                                <Text>
                                    {I18n.t(WorkplaceSecurityCheckAnswerTranslations[answer])}
                                </Text>
                                <Icon
                                    name={this.props.answer != null && this.props.answer === answer ? "check-square-o" : "square-o"}
                                    color={Theme.gray}
                                    size={38} />
                            </TouchableOpacity>
                        ))
                    }
                </View>
            </ScrollView>);
    }
}

const styles = StyleSheet.create({
    slide: {
        padding: Theme.screenMargin,
        backgroundColor: Theme.white,
    },
    textSection: {
        paddingBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: Theme.gray,
        fontSize: 20,
        fontWeight: 'bold',
    },
    answerSection: {
        flex: 1,
        flexDirection: 'row',
        alignSelf: 'stretch',
        justifyContent: 'space-between',
        paddingTop: 20,
        paddingBottom: 20
    },
    answer: {
        flex: 1,
        alignItems: 'center',
    }
})
