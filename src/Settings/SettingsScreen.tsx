import * as React from "react";
import { View, StyleSheet, Text, Modal, TextInput } from "react-native";
import { NavigationScreenProps, NavigationActions } from 'react-navigation';
import { Theme } from '../theme';
import I18n from 'react-native-i18n';
import { connect } from 'react-redux'
import { AppState, SupportedLocale, Target, ContactInfo, ReminderFrequency } from '../AppState';
import { ActionTypes, ChangeLocaleAction, CreateTargetAction, SetContactInfoAction, SetRepairReminderFrequencyAction, ToggleSafetyReminderAction } from '../Actions';
// import { LanguageSelection } from './LanguageSelection';
import { TargetSettings } from './TargetSettings';
import { IconButton } from '../Components/IconButton';
import { ContactInfoForm } from './ContactInfoForm';
import { ReminderSettingsForm } from './ReminderSettingsForm';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

interface Props extends NavigationScreenProps<{}> {
    selectedLang: SupportedLocale;
    targets: Array<Target>,
    currentContactInfo: ContactInfo,
    repairReminderFrequency: ReminderFrequency,
    isSafetyReminderEnabled: boolean,
    isSecurityReminderEnabled: boolean,
    changeLocale: (locale: SupportedLocale) => void;
    createTarget: (name: string, address: string, city: string) => void;
    saveContactInfo: (contactInfo: ContactInfo) => void;
    setRepairReminderFrequency: (frequency: ReminderFrequency) => void;
    toggleSafetyReminder: (isEnabled: boolean) => void;
    toggleSecurityReminder: (isEnabled: boolean) => void;
}

interface State {
    selectedLang: string;
    addTargetVisible: boolean;
    newTargetName: string;
    newTargetAddress: string;
    newTargetCity: string;
}

class SettingsScreen extends React.Component<Props, State> {

    static navigationOptions = ({ navigation, screenProps }) => {
        return ({
            title: navigation.state.params.title
        });
    }

    constructor(props: Props) {
        super(props)
        this.state = {
            selectedLang: 'fi',
            addTargetVisible: false,
            newTargetName: '',
            newTargetAddress: '',
            newTargetCity: ''
        };
    }

    selectLang(langCode: SupportedLocale) {
        this.setState({ selectedLang: langCode });
        I18n.locale = langCode;
        this.props.changeLocale(langCode);

        const { navigation } = this.props;
        const resetAction = NavigationActions.reset({
            index: 1,
            actions: [
                NavigationActions.navigate({ routeName: 'Home' }),
                NavigationActions.navigate({
                    routeName: 'Settings',
                    params: { title: I18n.t('settings.title') }
                })
            ]
        });
        navigation.dispatch(resetAction);
    }

    createNewTargetAndCloseModal() {
        this.props.createTarget(this.state.newTargetName, this.state.newTargetAddress, this.state.newTargetCity);
        this.closeModal();
    }

    closeModal() {
        this.setState({ addTargetVisible: false, newTargetName: '' });
    }

    render() {
        const { navigate } = this.props.navigation;
        const saveNewTargetDisabled = this.state.newTargetName == null || this.state.newTargetName.length === 0;

        return (
            <KeyboardAwareScrollView
                contentContainerStyle={styles.container}
                keyboardShouldPersistTaps="handled">
                <View style={styles.targetsSection}>
                    <Text style={styles.sectionTitle}>{I18n.t('settings.targetsSectionTitle')}</Text>
                    <TargetSettings
                        targets={this.props.targets}
                        onEditTarget={(targetId, name) => navigate('TargetDetails', { targetId, name })} />
                    <View style={{ padding: Theme.screenMargin }}>
                        <IconButton
                            icon="plus-square"
                            text={I18n.t('settings.addTarget')}
                            disabled={false}
                            color={Theme.gray}
                            onPress={() => this.setState({ addTargetVisible: true })} />
                    </View>
                </View>
                <View style={styles.reminderSection}>
                    <Text style={styles.sectionTitle}>{I18n.t('settings.reminderSectionTitle')}</Text>
                    <ReminderSettingsForm
                        repairReminderFrequency={this.props.repairReminderFrequency}
                        safetyReminderEnabled={this.props.isSafetyReminderEnabled}
                        securityReminderEnabled={this.props.isSecurityReminderEnabled}
                        onReminderFrequencyChange={(freq) => this.props.setRepairReminderFrequency(freq)}
                        onToggleSafetyReminder={isEnabled => this.props.toggleSafetyReminder(isEnabled)}
                        onToggleSecurityReminder={isEnabled => this.props.toggleSecurityReminder(isEnabled)} />
                </View>
                <View style={styles.contactSection}>
                    <Text style={styles.sectionTitle}>{I18n.t('settings.contactSectionTitle')}</Text>
                    <ContactInfoForm
                        contactInfo={this.props.currentContactInfo}
                        onContactInfoSave={info => this.props.saveContactInfo(info)} />
                </View>
                {/* <View style={styles.langSection}>
                    <Text style={styles.sectionTitle}>{I18n.t('settings.languagesSectionTitle')}</Text>
                    <LanguageSelection
                        selectedLang={this.props.selectedLang}
                        onChangeLocale={(lang) => this.selectLang(lang)} />
                </View> */}

                <Modal
                    animationType={"slide"}
                    transparent={false}
                    visible={this.state.addTargetVisible}
                    onRequestClose={() => this.closeModal()} >
                    <View style={styles.addNewTargetModal}>
                        <Text>{I18n.t('target.nameFieldTitle')}</Text>
                        <TextInput
                            style={styles.field}
                            autoFocus={true}
                            onSubmitEditing={() => { (this.refs.AddressInput as any).focus(); }}
                            returnKeyType="next"
                            value={this.state.newTargetName}
                            onChangeText={name => this.setState({ newTargetName: name })} />
                        <Text>{I18n.t('target.addressFieldTitle')}</Text>
                        <TextInput
                            ref="AddressInput"
                            onSubmitEditing={() => { (this.refs.CityInput as any).focus(); }}
                            returnKeyType="next"
                            style={styles.field}
                            value={this.state.newTargetAddress}
                            onChangeText={address => this.setState({ newTargetAddress: address })} />
                        <Text>{I18n.t('target.cityFieldTitle')}</Text>
                        <TextInput
                            ref="CityInput"
                            style={styles.field}
                            value={this.state.newTargetCity}
                            returnKeyType="done"
                            onChangeText={city => this.setState({ newTargetCity: city })} />
                        <View style={{ paddingTop: Theme.screenMargin }}>
                            <IconButton
                                icon="archive"
                                text={I18n.t('settings.saveNewTarget')}
                                disabled={saveNewTargetDisabled}
                                color={Theme.gray}
                                onPress={() => this.createNewTargetAndCloseModal()} />
                        </View>
                        <View style={{ paddingTop: Theme.screenMargin }}>
                            <IconButton
                                icon="arrow-left"
                                text={I18n.t('common.cancel')}
                                disabled={false}
                                color={Theme.red}
                                onPress={() => this.closeModal()} />
                        </View>
                    </View>
                </Modal>
            </KeyboardAwareScrollView>
        );
    }
}

const mapStateToProps = (state: AppState) => {
    return {
        selectedLang: state.currentLocale,
        targets: state.targets,
        currentContactInfo: state.contactInfo,
        repairReminderFrequency: state.repairReminderFrequency,
        isSafetyReminderEnabled: state.safetyReminderEnabled,
        isSecurityReminderEnabled: state.securityReminderEnabled
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeLocale: (locale: SupportedLocale) => {
            const action: ChangeLocaleAction = {
                type: ActionTypes.ChangeLocale,
                locale
            };
            dispatch(action);
        },
        createTarget: (name: string, address: string, city: string) => {
            const action: CreateTargetAction = {
                type: ActionTypes.CreateTarget,
                name, address, city
            };
            dispatch(action);
        },
        saveContactInfo: (contactInfo: ContactInfo) => {
            const action: SetContactInfoAction = {
                type: ActionTypes.SetContactInfo,
                contactInfo
            }
            dispatch(action);
        },
        setRepairReminderFrequency: (frequency: ReminderFrequency) => {
            const action: SetRepairReminderFrequencyAction = {
                type: ActionTypes.SetRepairReminderFrequency,
                frequency
            }
            dispatch(action);
        },
        toggleSafetyReminder: (isEnabled: boolean) => {
            const action: ToggleSafetyReminderAction = {
                type: ActionTypes.ToggleSafetyReminder,
                isEnabled
            }
            dispatch(action);
        },
        toggleSecurityReminder: (isEnabled: boolean) => {
            const action: ToggleSafetyReminderAction = {
                type: ActionTypes.ToggleSecurityReminder,
                isEnabled
            }
            dispatch(action);
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsScreen);

const screenMargin = 10;
const styles = StyleSheet.create({
    container: {
        alignSelf: 'stretch',
        backgroundColor: Theme.lightGray,
    },
    reminderSection: {
        alignSelf: 'stretch'
    },
    targetsSection: {
        alignSelf: 'stretch'
    },
    contactSection: {
        alignSelf: 'stretch'
    },
    langSection: {
        alignSelf: 'stretch',
        paddingBottom: Theme.screenMargin
    },
    sectionTitle: {
        fontSize: 18,
        color: Theme.gray,
        padding: screenMargin,
        paddingTop: 20,
    },
    addNewTargetModal: {
        flex: 1,
        paddingTop: 20,
        backgroundColor: Theme.lightGray,
        padding: Theme.screenMargin,
    },
    field: {
        flex: 0,
        fontSize: 20,
        marginBottom: 5
    }
});
