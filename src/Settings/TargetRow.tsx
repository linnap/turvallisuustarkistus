import * as React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Theme } from '../theme';
import Icon from 'react-native-vector-icons/FontAwesome';

interface Props {
    targetId: number;
    name: string;
    onEditTarget: (id: number, name: string) => void;
}

interface State {

}

export class TargetRow extends React.Component<Props, State> {
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    style={styles.item}
                    onPress={() => this.props.onEditTarget(this.props.targetId, this.props.name)}>
                    <Text style={{ fontSize: 20, color: Theme.gray, fontWeight: 'bold' }}>{this.props.name}</Text>
                    <Icon
                        name="arrow-right"
                        color={Theme.gray}
                        size={30} />
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    item: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        padding: 20,
        borderTopWidth: StyleSheet.hairlineWidth,
        borderColor: Theme.gray,
        borderStyle: 'solid',
    }
});
