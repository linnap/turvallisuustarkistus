import * as React from "react";
import { View, Image, StyleSheet, TouchableOpacity } from "react-native";
import { Theme } from '../theme';
import { SupportedLocale } from "../AppState";

interface Props {
    selectedLang: SupportedLocale;
    onChangeLocale: (locale: SupportedLocale) => void;
}

interface State {
}

export class LanguageSelection extends React.Component<Props, State> {

    selectLang(langCode: SupportedLocale) {
        this.props.onChangeLocale(langCode);
    }

    render() {
        const langs: Array<{ code: SupportedLocale, img: any }> = [
            { code: 'fi', img: require('../../images/fi.png') },
            { code: 'sv', img: require('../../images/sv.png') },
            { code: 'en', img: require('../../images/en.png') }
        ];

        return (
            <View style={styles.container}>
                <View style={styles.languages}>
                    {langs.map(lang =>
                        <TouchableOpacity
                            key={lang.code}
                            onPress={() => this.selectLang(lang.code)}>
                            <Image
                                source={lang.img}
                                style={this.props.selectedLang === lang.code ? styles.selected : styles.unselected} />
                        </TouchableOpacity>
                    )}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Theme.lightGray,
    },
    languages: {
        flex: 1,
        alignSelf: 'stretch',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    selected: {
        opacity: 1.0
    },
    unselected: {
        opacity: 0.2
    }
});
