
import * as React from 'react';
import { View, StyleSheet, Text, Switch, Slider } from "react-native";
import { Theme } from '../theme';
import { ReminderFrequency } from '../AppState';
import I18n from 'react-native-i18n';

interface Props {
    repairReminderFrequency: ReminderFrequency;
    safetyReminderEnabled: boolean;
    securityReminderEnabled: boolean;
    onReminderFrequencyChange: (reminderFrequency: ReminderFrequency) => void;
    onToggleSafetyReminder: (enabled: boolean) => void;
    onToggleSecurityReminder: (enabled: boolean) => void;
}

interface State {

}

const frequencyTranslations = {
    [ReminderFrequency.Off]: 'common.frequency.off',
    [ReminderFrequency.Daily]: 'common.frequency.daily',
    [ReminderFrequency.Weekly]: 'common.frequency.weekly',
    [ReminderFrequency.Monthly]: 'common.frequency.monthly',
}

export class ReminderSettingsForm extends React.Component<Props, State>{
    render() {
        return (
            <View style={styles.container}>
                <Text>{I18n.t('settings.remindToRepair')}:</Text>
                <Text style={styles.frequency}>
                    {I18n.t(frequencyTranslations[this.props.repairReminderFrequency])}
                </Text>
                <Slider
                    value={this.props.repairReminderFrequency}
                    maximumValue={3}
                    step={1}
                    onValueChange={value => this.props.onReminderFrequencyChange(value)} />
                <Text style={{ paddingTop: 20, paddingBottom: 20 }}>
                    {I18n.t('settings.sendYearlyReminder')}:
                </Text>
                <View style={styles.yearly}>
                    <Text>{I18n.t('safety.title')}</Text>
                    <Switch
                        value={this.props.safetyReminderEnabled}
                        onValueChange={value => this.props.onToggleSafetyReminder(value)} />
                </View>
                <View style={styles.yearly}>
                    <Text>{I18n.t('security.title')}</Text>
                    <Switch
                        value={this.props.securityReminderEnabled}
                        onValueChange={value => this.props.onToggleSecurityReminder(value)} />
                </View>
            </View >
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: Theme.screenMargin
    },
    frequency: {
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 20
    },
    yearly: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
})
