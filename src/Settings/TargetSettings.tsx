import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import { TargetRow } from './TargetRow';
import { Target } from '../AppState';

interface Props {
    targets: Array<Target>;
    onEditTarget: (id: number, name: string) => void;
}

interface State {

}

export class TargetSettings extends React.Component<Props, State> {
    render() {
        return (
            <View style={styles.container}>
                {this.props.targets.map(target => (
                    <View
                        key={target.id}>
                        <TargetRow
                            targetId={target.id}
                            name={target.name}
                            onEditTarget={(id, name) => this.props.onEditTarget(id, name)} />
                    </View>
                ))}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
});
