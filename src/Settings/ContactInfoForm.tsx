
import * as React from 'react';
import { View, StyleSheet, Text, TextInput } from "react-native";
import { Theme } from '../theme';
import { ContactInfo } from '../AppState';
import I18n from 'react-native-i18n';
import { IconButton } from '../Components/IconButton';

interface Props {
    contactInfo: ContactInfo;
    onContactInfoSave: (contactInfo: ContactInfo) => void;
}

interface State {
    name: string;
    address: string;
    city: string;
    zipCode: string;
    phoneNumber: string;
}

export class ContactInfoForm extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            name: props.contactInfo.name,
            address: props.contactInfo.address,
            city: props.contactInfo.city,
            zipCode: props.contactInfo.zipCode,
            phoneNumber: props.contactInfo.phoneNumber,
        }
    }

    render() {
        return (
            <View>
                <Text style={styles.contactFieldTitle}>{I18n.t('settings.contactFieldNameTitle')}</Text>
                <TextInput
                    style={styles.contactField}
                    value={this.state.name}
                    returnKeyType="next"
                    onSubmitEditing={() => { (this.refs.AddressInput as any).focus(); }}
                    onChangeText={name => this.setState({ name })} />
                <Text style={styles.contactFieldTitle}>{I18n.t('settings.contactFieldAddressTitle')}</Text>
                <TextInput
                    ref="AddressInput"
                    style={styles.contactField}
                    value={this.state.address}
                    returnKeyType="next"
                    onSubmitEditing={() => { (this.refs.ZipCodeInput as any).focus(); }}
                    onChangeText={address => this.setState({ address })} />
                <Text style={styles.contactFieldTitle}>{I18n.t('settings.contactFieldZipCodeTitle')}</Text>
                <TextInput
                    ref="ZipCodeInput"
                    style={styles.contactField}
                    value={this.state.zipCode}
                    returnKeyType="next"
                    onSubmitEditing={() => { (this.refs.CityInput as any).focus(); }}
                    keyboardType="numeric"
                    onChangeText={zipCode => this.setState({ zipCode })} />
                <Text style={styles.contactFieldTitle}>{I18n.t('settings.contactFieldCityTitle')}</Text>
                <TextInput
                    ref="CityInput"
                    style={styles.contactField}
                    value={this.state.city}
                    onSubmitEditing={() => { (this.refs.PhoneNumberInput as any).focus(); }}
                    returnKeyType="next"
                    onChangeText={city => this.setState({ city })} />
                <Text style={styles.contactFieldTitle}>{I18n.t('settings.contactFieldPhoneNumberTitle')}</Text>
                <TextInput
                    ref="PhoneNumberInput"
                    style={styles.contactField}
                    value={this.state.phoneNumber}
                    keyboardType="phone-pad"
                    returnKeyType="done"
                    onChangeText={phoneNumber => this.setState({ phoneNumber })} />
                <View style={{ padding: Theme.screenMargin }}>
                    <IconButton
                        icon="archive"
                        text={I18n.t('settings.saveContactInfo')}
                        disabled={false}
                        color={Theme.gray}
                        onPress={() => this.props.onContactInfoSave({
                            name: this.state.name,
                            address: this.state.address,
                            zipCode: this.state.zipCode,
                            city: this.state.city,
                            phoneNumber: this.state.phoneNumber
                        })} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    contactFieldTitle: {
        fontSize: 12,
        color: Theme.gray,
        paddingLeft: Theme.screenMargin,
        paddingTop: 10,
    },
    contactField: {
        flex: 1,
        fontSize: 16,
        margin: Theme.screenMargin,
        marginTop: 0,
    },
})
