import {
    initialSafetyChecklist,
    SafetyCheckItem,
    SafetyCheckAnswer,
    SafetyCheckPhoto
} from './SafetyCheck/SafetyChecklist';
import { 
    SecurityCheckItem,
    SecurityCheckAnswer,
    initialSecurityChecklist
} from './SecurityCheck/SecurityChecklist';
import { 
    SecurityWalkCheckItem,
    SecurityWalkCheckAnswer,
    initialSecurityWalkChecklist
} from './SecurityWalkCheck/SecurityWalkChecklist';
import { 
    WorkplaceSecurityCheckItem,
    WorkplaceSecurityCheckAnswer,
    initialWorkplaceSecurityChecklist
} from './WorkplaceSecurityCheck/WorkplaceSecurityChecklist';

export type SupportedLocale = 'fi' | 'sv' | 'en';

export interface AppState {
    currentLocale: SupportedLocale;
    contactInfo: ContactInfo;
    targets: Array<Target>;
    selectedTarget?: Target;
    safetyChecklist: Array<SafetyCheckItem>;
    safetyCheckAnswers: Array<SafetyCheckAnswer>;
    safetyCheckPhotos: Array<SafetyCheckPhoto>;
    securityChecklist: Array<SecurityCheckItem>;
    securityCheckAnswers: Array<SecurityCheckAnswer>;
    securityWalkChecklist: Array<SecurityWalkCheckItem>;
    securityWalkCheckAnswers: Array<SecurityWalkCheckAnswer>;
    workplaceSecurityChecklist: Array<WorkplaceSecurityCheckItem>;
    workplaceSecurityCheckAnswers: Array<WorkplaceSecurityCheckAnswer>;
    repairReminderFrequency: ReminderFrequency;
    safetyReminderEnabled: boolean;
    safetyReminderEnabledTimestamp?: Date | null;
    securityReminderEnabled: boolean;
    securityReminderEnabledTimestamp?: Date | null;
/* TODO SecurityWalkCheck and WorkplaceSecurityCheck reminders ? */
}

export enum ReminderFrequency {
    Off = 0, Daily = 1, Weekly = 2, Monthly = 3
}

export interface ContactInfo {
    name: string;
    address: string;
    city: string;
    zipCode: string;
    phoneNumber: string;
}

export interface Target {
    id: number,
    name: string;
    address: string;
    city: string;
}

export const initialState: AppState = {
    currentLocale: 'fi',
    contactInfo: {
        address: '',
        city: '',
        name: '',
        zipCode: '',
        phoneNumber: ''
    },
    targets: [],
    safetyChecklist: initialSafetyChecklist,
    safetyCheckAnswers: [],
    safetyCheckPhotos: [],
    securityChecklist: initialSecurityChecklist,
    securityCheckAnswers: [],
    securityWalkChecklist: initialSecurityWalkChecklist,
    securityWalkCheckAnswers: [],
    workplaceSecurityChecklist: initialWorkplaceSecurityChecklist,
    workplaceSecurityCheckAnswers: [],
    repairReminderFrequency: ReminderFrequency.Off,
    safetyReminderEnabled: false,
    securityReminderEnabled: false
};
