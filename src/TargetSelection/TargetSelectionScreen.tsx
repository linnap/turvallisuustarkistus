import * as React from "react";
import { View, StyleSheet, Text, ScrollView, TouchableOpacity } from "react-native";
import { NavigationScreenProps, NavigationParams, NavigationActions } from 'react-navigation';
import { Theme } from '../theme';
import { connect } from 'react-redux';
import { AppState, Target } from "../AppState";
import { IconButton } from "../Components/IconButton";
import Icon from 'react-native-vector-icons/FontAwesome';
import I18n from 'react-native-i18n';
import { ActionTypes, SelectTargetAction } from "../Actions";

interface Props extends NavigationScreenProps<NavigationParams> {
    targets: Array<Target>;
    selectTarget: (target: Target) => void;
}

interface State {

}

export class TargetSelectionScreen extends React.Component<Props, State> {

    static navigationOptions = ({ navigation, screenProps }) => {
        return ({
            title: navigation.state.params.title
        })
    };

    selectTargetAndNavigate(target: Target) {
        this.props.selectTarget(target);
        this.props.navigation.navigate(this.props.navigation.state.params.target, {
            title: target.name,
            showList: false,
            hasOneTarget: false,
            checkIndex: 0
        });
    }

    navigateToSettings() {
        const resetAction = NavigationActions.reset({
            index: 1,
            actions: [
                NavigationActions.navigate({
                    routeName: 'Home'
                }),
                NavigationActions.navigate({
                    routeName: 'Settings',
                    params: { title: I18n.t('settings.title') }
                })
            ]
        });
        this.props.navigation.dispatch(resetAction);
    }

    render() {
        return (
            <View style={styles.container}>
                {this.props.targets.length === 0 ?
                    <View style={{ padding: Theme.screenMargin }}>
                        <Text style={{ fontSize: 16, paddingBottom: 10 }}>{I18n.t('common.noTargetsDefined')}</Text>
                        <IconButton
                            color={Theme.gray}
                            disabled={false}
                            icon="cog"
                            text={I18n.t('home.nav.settings')}
                            onPress={() => this.navigateToSettings()} />
                    </View> :
                    <ScrollView contentContainerStyle={styles.targetContainer}>
                        <Text style={{ fontSize: 20, textAlign: 'center', padding: Theme.screenMargin }}>
                            {I18n.t('targetSelection.selectTarget')}
                        </Text>
                        {this.props.targets.map(target =>
                            <TouchableOpacity
                                key={target.id}
                                style={styles.targetModalItem}
                                onPress={() => this.selectTargetAndNavigate(target)}>
                                <Text style={{ fontSize: 20, color: Theme.gray, fontWeight: 'bold' }}>{target.name}</Text>
                                <Icon
                                    name="arrow-right"
                                    color={Theme.gray}
                                    size={30} />
                            </TouchableOpacity>
                        )}
                        <View style={{ padding: Theme.screenMargin }}>
                            <IconButton
                                icon="arrow-left"
                                color={Theme.gray}
                                disabled={false}
                                text={I18n.t('common.back')}
                                onPress={() => this.props.navigation.goBack()} />
                        </View>
                    </ScrollView>
                }
            </View>
        );
    }
}

const mapStateToProps = (state: AppState) => {
    return {
        targets: state.targets
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        selectTarget: (target: Target) => {
            const action: SelectTargetAction = {
                type: ActionTypes.SelectTarget,
                target
            };
            dispatch(action);
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(TargetSelectionScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Theme.white,
    },
    targetContainer: {
        backgroundColor: Theme.white,
    },
    targetModalItem: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        padding: 20,
        borderTopWidth: StyleSheet.hairlineWidth,
        borderColor: Theme.gray,
        borderStyle: 'solid',
    },
});
