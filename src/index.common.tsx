import * as React from 'react';
import { AppRegistry, AsyncStorage } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { persistStore, autoRehydrate } from 'redux-persist'
import I18n from 'react-native-i18n';

import './i18n';

import HomeScreen from './Home/HomeScreen';
import SafetyCheckScreen from './SafetyCheck/SafetyCheckScreen';
import SecurityCheckScreen from './SecurityCheck/SecurityCheckScreen';
import SecurityWalkCheckScreen from './SecurityWalkCheck/SecurityWalkCheckScreen';
import WorkplaceSecurityCheckScreen from './WorkplaceSecurityCheck/WorkplaceSecurityCheckScreen';
import SettingsScreen from './Settings/SettingsScreen';
import TargetDetailsScreen from './TargetDetails/TargetDetailsScreen';
import TargetSelectionScreen from './TargetSelection/TargetSelectionScreen';
import ResultsScreen from './Results/ResultsScreen';
import { AppInfoScreen } from './AppInfo/AppInfoScreen';
import { appStateReducer } from './AppStateReducer';

const App = StackNavigator({
    Home: {
        screen: HomeScreen,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    },
    Safety: { screen: SafetyCheckScreen },
    Security: { screen: SecurityCheckScreen },
    SecurityWalk: { screen: SecurityWalkCheckScreen },
    WorkplaceSecurityCheck: { screen: WorkplaceSecurityCheckScreen },
    Settings: { screen: SettingsScreen },
    Results: { screen: ResultsScreen },
    AppInfo: { screen: AppInfoScreen },
    TargetDetails: { screen: TargetDetailsScreen },
    TargetSelection: { screen: TargetSelectionScreen }
});

import * as PushNotification from 'react-native-push-notification';
PushNotification.configure({});

const store = createStore(appStateReducer, undefined, autoRehydrate());

persistStore(store, { storage: AsyncStorage });
AsyncStorage.removeItem('reduxPersist:safetyChecklist');
AsyncStorage.removeItem('reduxPersist:securityChecklist');
AsyncStorage.removeItem('reduxPersist:securityWalkChecklist');
AsyncStorage.removeItem('reduxPersist:workplaceSecurityChecklist');

const state = store.getState();
I18n.locale = state ? state.currentLocale : 'en';

const turvallisuustarkistus = () => (
    <Provider store={store}>
        <App />
    </Provider>
);

AppRegistry.registerComponent('turvallisuustarkistus', () => turvallisuustarkistus);
