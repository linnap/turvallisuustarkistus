import { sortBy } from 'lodash';

export interface SecurityCheckItem {
    id: number;
    sortIndex: number;
    text: string;
    infoText?: string;
}


export interface SecurityCheckAnswer {
    targetId: number;
    checkItemId: number;
    answer: SecurityCheckAnswerType;
}

export enum SecurityCheckAnswerType {
    Ok = 'OK', NotOk = 'NOK', NoAction = 'NO_ACTION'
}

export const SecurityCheckAnswerTranslations: { [answer: number]: string } = {
    [SecurityCheckAnswerType.NotOk]: 'security.answerNotOk',
    [SecurityCheckAnswerType.Ok]: 'security.answerOk',
    [SecurityCheckAnswerType.NoAction]: 'security.answerNoAction',
}

export const initialSecurityChecklist: Array<SecurityCheckItem> = sortBy([
    {
        id: 0,
        sortIndex: 0,
        text: 'Perehtyminen tietoteknisten laitteiden ohjeistuksiin ja toimintoihin'
    },
    {
        id: 1,
        sortIndex: 1,
        text: 'Internetyhteyden/ADSL-modeemin salasana vaihdettu pois tehdasoletuksesta'
    },
    {
        id: 2,
        sortIndex: 2,
        text: 'Langattomien verkkojen ja laitteiden tietoturva ja salaus kunnossa'
    },
    {
        id: 3,
        sortIndex: 3,
        text: 'Omissa tietokoneissa ja laitteissa jokaisella käyttäjällä oma tunnus ja salasana '
    },
    {
        id: 4,
        sortIndex: 4,
        text: 'Yksityisyyden suoja huomioitu, ei tarpeettomasti luovuteta omia tietoja'
    },
    {
        id: 5,
        sortIndex: 5,
        text: 'Varmuuskopiointia suoritetaan aktiivisesti omista laitteista ja tiedoista'
    },
    {
        id: 6,
        sortIndex: 6,
        text: 'Postitse tulleista kirjeistä/tarjouksista yms. poistetaan omat tiedot ennen kierrätystä'
    },
    {
        id: 7,
        sortIndex: 7,
        text: 'Rahaliikenteen turvallisuuden tarkistus (verkon kautta tehdyt nettiostokset jne.)'
    },
    {
        id: 8,
        sortIndex: 8,
        text: 'Tarpeettomien ja vanhojen laitteiden tiedot tuhottu ennen kierrätykseen laittoa'
    },
    {
        id: 9,
        sortIndex: 9,
        text: 'Suunnitelma, miten toimin jos koneeseeni, yksityisyyteeni tai tietosuojaani kohdistuu uhkia'
    }], (item) => item.sortIndex);

