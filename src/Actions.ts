import { SupportedLocale, ContactInfo, ReminderFrequency, Target } from './AppState';
import { SafetyCheckAnswerType } from './SafetyCheck/SafetyChecklist';
import { SecurityCheckAnswerType } from './SecurityCheck/SecurityChecklist';
import { SecurityWalkCheckAnswerType } from './SecurityWalkCheck/SecurityWalkChecklist';
import { WorkplaceSecurityCheckAnswerType } from './WorkplaceSecurityCheck/WorkplaceSecurityChecklist';

export enum ActionTypes {
    SelectTarget = 'SELECT_TARGET',
    // Safety check
    SetSafetyCheckAnswer = 'SET_SAFETY_ANSWER',
    AddPhotoToSafetyCheck = 'ADD_PHOTO_TO_SAFETY_CHECK',
    RemovePhotoFromSafetyCheck = 'REMOVE_PHOTO_FROM_SAFETY_CHECK',
    // Security check
    SetSecurityCheckAnswer = 'SET_SECURITY_ANSWER',
    // SecurityWalk check
    SetSecurityWalkCheckAnswer = 'SET_SECURITY_WALK_ANSWER',
    // WorkplaceSecurityCheck check
    SetWorkplaceSecurityCheckAnswer = 'SET_WORKPLACE_SECURITY_CHECK_ANSWER',
    // Settings
    ChangeLocale = 'CHANGE_LOCALE',
    RemoveTarget = 'REMOVE_TARGET',
    UpdateTarget = 'UPDATE_TARGET_NAME',
    CreateTarget = 'CREATE_TARGET',
    SetContactInfo = 'SET_CONTACT_INFO',
    SetRepairReminderFrequency = 'SET_REPAIR_REMINDER_FREQUENCY',
    ToggleSafetyReminder = 'TOGGLE_SAFETY_REMINDER',
    ToggleSecurityReminder = 'TOGGLE_SECURITY_REMINDER',
    // Results
    ClearResults = 'CLEAR_RESULTS',
    SendPrivateResults = 'SEND_PRIVATE_RESULTS',
    SendPublicResults = 'SEND_PUBLIC_RESULTS'
}

export interface Action {
    type: ActionTypes;
}

export interface ChangeLocaleAction extends Action {
    locale: SupportedLocale;
}

export interface RemoveTargetAction extends Action {
    targetId: number;
}

export interface UpdateTargetAction extends Action {
    targetId: number;
    name: string;
    address: string;
    city: string;
}

export interface CreateTargetAction extends Action {
    name: string;
    address: string;
    city: string;
}

export interface SetSafetyCheckAnswerAction extends Action {
    targetId: number;
    checkItemId: number;
    answer: SafetyCheckAnswerType;
}

export interface AddPhotoToSafetyCheckAction extends Action {
    targetId: number;
    checkItemId: number;
    photoUri: string;
}

export interface RemovePhotoFromSafetyCheckAction extends Action {
    targetId: number;
    checkItemId: number;
    photoUri: string;
}

export interface SetContactInfoAction extends Action {
    contactInfo: ContactInfo;
}

export interface SetRepairReminderFrequencyAction extends Action {
    frequency: ReminderFrequency;
}

export interface ToggleSafetyReminderAction extends Action {
    isEnabled: boolean;
}

export interface ToggleSecurityReminderAction extends Action {
    isEnabled: boolean;
}

export interface SelectTargetAction extends Action {
    target: Target;
}

export interface ClearResultsAction extends Action {
    targetId: number;
}

export interface SendPrivateResultsAction extends Action {
}

export interface SendPublicResultsAction extends Action {
}

export interface SetSecurityCheckAnswerAction extends Action {
    targetId: number;
    checkItemId: number;
    answer: SecurityCheckAnswerType;
}

export interface SetSecurityWalkCheckAnswerAction extends Action {
    targetId: number;
    checkItemId: number;
    answer: SecurityWalkCheckAnswerType;
}

export interface SetWorkplaceSecurityCheckAnswerAction extends Action {
    targetId: number;
    checkItemId: number;
    answer: WorkplaceSecurityCheckAnswerType;
}
