import * as React from "react";
import { View, StyleSheet, Text, TouchableOpacity, Modal, ScrollView } from "react-native";
import { NavigationScreenProps, NavigationActions } from 'react-navigation';
import { Theme } from '../theme';
import SwipeableViews from 'react-swipeable-views-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { IconButton } from '../Components/IconButton';
import I18n from 'react-native-i18n';
import { connect } from 'react-redux';
import { SecurityWalkCheckAnswerType, SecurityWalkCheckAnswer, SecurityWalkCheckItem } from './SecurityWalkChecklist';
import { ActionTypes, SetSecurityWalkCheckAnswerAction } from '../Actions';
import { AppState, Target } from '../AppState';
import { findIndex, find } from 'lodash';
import { SecurityWalkChecklistSlide } from './SecurityWalkChecklistSlide';

interface NavigationParams {
    title: string;
    showList: boolean;
    checkIndex: number;
    hasOneTarget: boolean;
}

interface Props extends NavigationScreenProps<NavigationParams> {
    checklist: Array<SecurityWalkCheckItem>;
    answers: Array<SecurityWalkCheckAnswer>;
    selectedTarget: Target;
    setSecurityWalkAnswer: (targetId: number, checkId: number, answer: SecurityWalkCheckAnswerType) => void;
}

interface State {
    isListVisible: boolean;
    currentCheckIndex: number;
    selectedTarget: Target;
}

class SecurityWalkCheckScreen extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            isListVisible: props.navigation.state.params.showList,
            currentCheckIndex: props.navigation.state.params.checkIndex,
            selectedTarget: props.selectedTarget
        };
    }

    static navigationOptions = ({ navigation, screenProps }) => {
        return ({
            title: navigation.state.params.title,
            headerRight: <TouchableOpacity
                style={{ paddingRight: Theme.screenMargin }}
                onPress={() => {
                    const actions = [
                        NavigationActions.navigate({ routeName: 'Home' })];

                    // If there's more than one target defined, add target selection to the path
                    if (!navigation.state.params.hasOneTarget) {
                        actions.push(NavigationActions.navigate({
                            routeName: 'TargetSelection',
                            params: {
                                title: I18n.t('securityWalk.title'),
                                target: 'SecurityWalk'
                            }
                        }));
                    }

                    actions.push(NavigationActions.navigate({
                        routeName: 'SecurityWalk',
                        params: {
                            title: navigation.state.params.title,
                            checkIndex: navigation.state.params.checkIndex,
                            hasOneTarget: navigation.state.params.hasOneTarget,
                            showList: true
                        }
                    }));

                    const resetAction = NavigationActions.reset({
                        index: actions.length - 1, actions
                    });
                    navigation.dispatch(resetAction);
                }}>
                <Icon name="list" size={20} color={Theme.black} />
            </TouchableOpacity>
        });
    };

    closeListModal() {
        const { navigation } = this.props;
        const actions = [NavigationActions.navigate({ routeName: 'Home' })];

        // If there's more than one target defined, add target selection to the path
        if (!navigation.state.params.hasOneTarget) {
            actions.push(NavigationActions.navigate({
                routeName: 'TargetSelection',
                params: {
                    title: I18n.t('securityWalk.title'),
                    target: 'SecurityWalk'
                }
            }));
        }

        actions.push(NavigationActions.navigate({
            routeName: 'SecurityWalk',
            params: {
                title: navigation.state.params.title,
                checkIndex: this.state.currentCheckIndex,
                hasOneTarget: navigation.state.params.hasOneTarget,
                showList: false
            }
        }));

        const resetAction = NavigationActions.reset({
            index: actions.length - 1, actions
        });
        navigation.dispatch(resetAction);
    }

    componentWillReceiveProps(nextProps: Props) {
        this.setState({
            isListVisible: nextProps.navigation.state.params.showList,
            currentCheckIndex: nextProps.navigation.state.params.checkIndex
        });
    }

    goToItem(checkId: number) {
        const index = findIndex(this.props.checklist, item => item.id === checkId);
        this.setState({ currentCheckIndex: index }, () => this.closeListModal());
    }

    onSlideChange(index: number) {
        this.props.navigation.setParams({ checkIndex: index });
        this.setState({ currentCheckIndex: index })
    }

    navigateToResults() {
        const { navigation } = this.props;
        const resetAction = NavigationActions.reset({
            index: 1,
            actions: [
                NavigationActions.navigate({ routeName: 'Home' }),
                NavigationActions.navigate({
                    routeName: 'Results',
                    params: { title: I18n.t('results.title') }
                })
            ]
        });
        navigation.dispatch(resetAction);
    }

    navigateToHome() {
        const { navigation } = this.props;
        const resetAction = NavigationActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Home' })]
        });
        navigation.dispatch(resetAction);
    }

    render() {
        return (
            <View style={styles.container}>
                <SwipeableViews
                    resistance={true}
                    index={this.state.currentCheckIndex}
                    onChangeIndex={index => this.onSlideChange(index)}>
                    {this.props.checklist.map((check, index) => {
                        const targetId = this.state.selectedTarget.id;
                        const checkAnswer = find(this.props.answers, a => a.targetId === targetId && a.checkItemId === check.id);
                        return (
                            <SecurityWalkChecklistSlide
                                key={check.id}
                                index={index + 1}
                                text={check.text}
                                infoText={check.infoText}
                                answer={checkAnswer != null ? checkAnswer.answer : undefined}
                                onAnswerClick={answer => this.props.setSecurityWalkAnswer(targetId, check.id, answer)} />
                        );
                    })}
                    <View style={styles.doneSlide}>
                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name="check-circle" size={124} color="green" />
                            <Text style={styles.doneSlideText}>{I18n.t('common.checkDone')}</Text>
                        </View>
                        <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'space-around' }}>
                            <Icon.Button
                                name="file-text-o"
                                backgroundColor={Theme.white}
                                color={Theme.gray}
                                size={36}
                                onPress={() => this.navigateToResults()}>
                                <Text style={{ fontSize: 20, color: Theme.gray }}>{I18n.t('home.nav.results')}</Text>
                            </Icon.Button>
                            <Icon.Button
                                name="home"
                                backgroundColor={Theme.white}
                                color={Theme.gray}
                                size={36}
                                onPress={() => this.navigateToHome()}>
                                <Text style={{ fontSize: 20, color: Theme.gray }}>{I18n.t('home.title')}</Text>
                            </Icon.Button>
                        </View>
                    </View>
                </SwipeableViews>

                {/* QUICK NAVIGATION MODAL */}
                <Modal
                    animationType={'slide'}
                    transparent={false}
                    visible={this.state.isListVisible}
                    onRequestClose={() => this.closeListModal()}>
                    <ScrollView>
                        <View style={{ padding: Theme.screenMargin, paddingTop: 20 }}>
                            <IconButton
                                icon="arrow-left"
                                color={Theme.gray}
                                disabled={false}
                                text={I18n.t('common.back')}
                                onPress={() => this.closeListModal()} />
                        </View>

                        {this.props.checklist.map((check, index) => {
                            return (
                                <TouchableOpacity
                                    key={check.id}
                                    onPress={() => this.goToItem(check.id)}>
                                    <Text style={styles.listModalItem}>
                                        {`${index + 1}. ${check.text}`}
                                    </Text>
                                </TouchableOpacity>);
                        })}

                        <View style={{ padding: Theme.screenMargin }}>
                            <IconButton
                                icon="arrow-left"
                                color={Theme.gray}
                                disabled={false}
                                text={I18n.t('common.back')}
                                onPress={() => this.closeListModal()} />
                        </View>
                    </ScrollView>
                </Modal>
            </View >
        );
    }
}

const mapStateToProps = (state: AppState) => {
    return {
        checklist: state.securityWalkChecklist,
        answers: state.securityWalkCheckAnswers,
        selectedTarget: state.selectedTarget
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setSecurityWalkAnswer: (targetId: number, checkItemId: number, answer: SecurityWalkCheckAnswerType) => {
            const action: SetSecurityWalkCheckAnswerAction = {
                type: ActionTypes.SetSecurityWalkCheckAnswer,
                targetId, checkItemId, answer
            };
            dispatch(action);
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SecurityWalkCheckScreen)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Theme.white
    },
    text: {
        color: Theme.gray,
        fontSize: 30,
        fontWeight: 'bold',
    },
    listModalItem: {
        padding: Theme.screenMargin,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: Theme.gray,
        color: Theme.gray
    },
    doneSlide: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: Theme.screenMargin
    },
    doneSlideText: {
        fontSize: 28,
        color: 'green'
    }
})
