import { sortBy } from 'lodash';

export interface SecurityWalkCheckItem {
    id: number;
    sortIndex: number;
    text: string;
    infoText?: string;
}

export interface SecurityWalkCheckAnswer {
    targetId: number;
    checkItemId: number;
    answer: SecurityWalkCheckAnswerType;
}

export enum SecurityWalkCheckAnswerType {
    Ok = 'OK', NotOk = 'NOK'
}

export const SecurityWalkCheckAnswerTranslations: { [answer: number]: string } = {
    [SecurityWalkCheckAnswerType.NotOk]: 'securityWalk.answerNotOk',
    [SecurityWalkCheckAnswerType.Ok]: 'securityWalk.answerOk'
}

export const initialSecurityWalkChecklist: Array<SecurityWalkCheckItem> = sortBy([
    {
        id: 0,
        sortIndex: 0,
        text: 'Hätätilanteen ohje ja poistumiskartat sijoitettu asianmukaisesti'
    },
    {
        id: 1,
        sortIndex: 1,
        text: 'Yleisissä tiloissa poistumiskartat ja toimintaohjeet'
    },
    {
        id: 2,
        sortIndex: 2,
        text: 'Sammuttimet kunnossa ja niiden edusta vapaa'
    },
    {
        id: 3,
        sortIndex: 3,
        text: 'Pikapalopostit kunnossa ja niiden edusta vapaa'
    },
    {
        id: 4,
        sortIndex: 4,
        text: 'Sammutuspeitteet kunnossa ja niiden edusta vapaa'
    },
    {
        id: 5,
        sortIndex: 5,
        text: 'Hätäpoistumistiet: valot toimivat ja merkinnät selkeästi näkyvissä'
    },
    {
        id: 6,
        sortIndex: 6,
        text: 'Poistumisreitit vapaat, reittien ovet avattavissa ilman avainta'
    },
    {
        id: 7,
        sortIndex: 7,
        text: 'Osastoivat palo-ovet kiinni ja itsestään salpautuvat'
    },
    {
        id: 8,
        sortIndex: 8,
        text: 'Ensiapukaapeissa tarpeiden mukaiset varusteet ja tarkastettu'
    },
    {
        id: 9,
        sortIndex: 9,
        text: 'Tilat järjestyksessä ja siistit, kulkureitit vapaat'
    },
    {
        id: 10,
        sortIndex: 10,
        text: 'Ei putoamisvaaraa, kaiteet turvalliset'
    },
    {
        id: 11,
        sortIndex: 11,
        text: 'Lattialla ei liukastumisvaaraa, matot eivät luista'
    },
    {
        id: 12,
        sortIndex: 12,
        text: 'Valaistus on riittävä, vialliset lamput vaihdettu'
    },
    {
        id: 13,
        sortIndex: 13,
        text: 'Ei ole meluhaittoja/Suojaimia käytetään aina tarvittaessa'
    },
    {
        id: 14,
        sortIndex: 14,
        text: 'Ei kosteutta, lämpötila oikea ja ilmanvaihto toimii'
    },
    {
        id: 15,
        sortIndex: 15,
        text: 'Varoetäisyydet kuumista laitteista palaviin materiaaleihin riittävät'
    },
    {
        id: 16,
        sortIndex: 16,
        text: 'Vaaralliset/palavat aineet: säilytys ja käsittely ohjeiden mukaista'
    },
    {
        id: 17,
        sortIndex: 17,
        text: 'Tulityöskentelyyn liittyvien tilojen ohjeet asianmukaiset'
    },
    {
        id: 18,
        sortIndex: 18,
        text: 'Koneet ja laitteet tarkastettu, ne ovat toimivia ja turvallisia'
    },
    {
        id: 19,
        sortIndex: 19,
        text: 'Laitteiden ja koneiden käyttöperehdytys on toteutettu'
    },
    {
        id: 20,
        sortIndex: 20,
        text: 'Suojavaatetusta ja suojavarusteita käytetään aina asianmukaisesti'
    },
    {
        id: 21,
        sortIndex: 21,
        text: 'Rakennuksen kokoontumispaikka asianmukainen'
    },
    {
        id: 22,
        sortIndex: 22,
        text: 'Pelastustiet auki (min. leveys 3,5 m)'
    },
    {
        id: 23,
        sortIndex: 23,
        text: 'Piha-alueen liikenne ja pysäköinti ohjeiden mukaista'
    },
    {
        id: 24,
        sortIndex: 24,
        text: 'Alue siisti, jäteastiat kunnossa ja pois seinien vierestä'
    },
    {
        id: 25,
        sortIndex: 25,
        text: 'Portaat ja kulkuväylät ovat puhtaat ja liukkaus on estetty'
    }], (item) => item.sortIndex);

