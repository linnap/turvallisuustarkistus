import * as React from "react";
import { View, StyleSheet, Text, ScrollView, TouchableOpacity } from "react-native";
import { NavigationScreenProps, NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { Theme } from '../theme';
import { AppState, Target } from "../AppState";
import I18n from 'react-native-i18n';
import { IconButton } from "../Components/IconButton";
import { SafetyCheckAnswerType, SafetyCheckItem } from "../SafetyCheck/SafetyChecklist";
import Icon from 'react-native-vector-icons/FontAwesome';
import * as _ from 'lodash';
import { ActionTypes, SendPrivateResultsAction, SendPublicResultsAction, SelectTargetAction } from "../Actions";
import { SecurityCheckItem } from "../SecurityCheck/SecurityChecklist";
import { SecurityWalkCheckItem } from "../SecurityWalkCheck/SecurityWalkChecklist";
import { WorkplaceSecurityCheckItem } from "../WorkplaceSecurityCheck/WorkplaceSecurityChecklist";


interface Fixable {
    checkItemId: number;
    target: Target;
    text: string;
}

interface Props extends NavigationScreenProps<{}> {
    targets: Target[];
    selectedTarget: Target;
    safetyThingsToFix: Fixable[];
    securityThingsToFix: Fixable[];
    securityWalkThingsToFix: Fixable[];
    workplaceSecurityCheckThingsToFix: Fixable[];
    safetyCheckItems: Array<SafetyCheckItem>;
    securityCheckItems: Array<SecurityCheckItem>;
    securityWalkCheckItems: Array<SecurityWalkCheckItem>;
    workplaceSecurityCheckItems: Array<WorkplaceSecurityCheckItem>;
    sendPrivateResults: () => void;
    sendPublicResults: () => void;
    selectTarget: (target: Target) => void;
}

class ResultsScreen extends React.Component<Props, {}> {

    static navigationOptions = ({ navigation, screenProps }) => {
        return ({
            title: navigation.state.params.title
        })
    };

    constructor(props: Props) {
        super(props);
    }

    navigateToSettings() {
        const resetAction = NavigationActions.reset({
            index: 1,
            actions: [
                NavigationActions.navigate({
                    routeName: 'Home'
                }),
                NavigationActions.navigate({
                    routeName: 'Settings',
                    params: { title: I18n.t('settings.title') }
                })
            ]
        });
        this.props.navigation.dispatch(resetAction);
    }

    navigateToCheckItem(routeName: string, target: Target, checkItemId: number) {
        this.props.selectTarget(target);
        this.props.navigation.navigate(routeName, {
            title: target.name,
            showList: false,
            hasOneTarget: false,
            checkIndex: this.props.safetyCheckItems.findIndex(i => i.id === checkItemId)
        });
    }

    render() {
        if (this.props.targets.length === 0) {
            return (
                <View style={{ padding: Theme.screenMargin }}>
                    <Text style={{ fontSize: 16, paddingBottom: 10 }}>{I18n.t('common.noTargetsDefined')}</Text>
                    <IconButton
                        color={Theme.gray}
                        disabled={false}
                        icon="cog"
                        text={I18n.t('home.nav.settings')}
                        onPress={() => this.navigateToSettings()} />
                </View>);
        }

        const selectedTarget = this.props.selectedTarget || this.props.targets[0];

        const noFixablesComponent = (
            <View style={styles.noFixables}>
                <Icon name="check" color="green" size={24} />
                <Text style={{ paddingLeft: 10 }}>{I18n.t('results.noFixables')}</Text>
            </View>
        );

        const safetyFixables = this.props.safetyThingsToFix.filter(fixable => fixable.target.id === selectedTarget.id);
        const securityFixables = this.props.securityThingsToFix.filter(fixable => fixable.target.id === selectedTarget.id);
        const securityWalkFixables = this.props.securityWalkThingsToFix.filter(fixable => fixable.target.id === selectedTarget.id);
        const workplaceSecurityCheckFixables = this.props.workplaceSecurityCheckThingsToFix.filter(fixable => fixable.target.id === selectedTarget.id);

        return (
            <View style={styles.container}>
                <ScrollView contentContainerStyle={styles.resultsContainer}>
                    <Text style={styles.title}>
                        {I18n.t('results.selectTarget')}
                    </Text>

                    {this.props.targets.map(target =>
                        (<TouchableOpacity
                            style={[styles.targetRow, selectedTarget.id === target.id && styles.selectedTargetRow]}
                            key={target.id}
                            onPress={() => this.props.selectTarget(target)}>
                            <Text style={{ fontSize: 20, fontWeight: 'bold', color: selectedTarget.id === target.id ? Theme.white : Theme.gray }}>{target.name}</Text>
                        </TouchableOpacity>)
                    )}

                    <View style={{ marginBottom: 20 }}>
                        <Text style={styles.title}>
                            {I18n.t('results.thingsToFix')}
                        </Text>
                        <Text style={{ fontSize: 16, marginBottom: 10 }}>
                            {I18n.t('results.safety')}
                        </Text>
                        {safetyFixables.length > 0 ?
                            safetyFixables.map(fixable => (
                                <View key={fixable.checkItemId}
                                    style={styles.fixableRow}>
                                    <Text style={styles.fixableRowText}>{fixable.text}</Text>
                                    <TouchableOpacity
                                        style={styles.fixableRowButton}
                                        onPress={() => this.navigateToCheckItem('Safety', fixable.target, fixable.checkItemId)}>
                                        <Icon
                                            name="arrow-right"
                                            color={Theme.gray}
                                            size={30} />
                                    </TouchableOpacity>
                                </View>)) : noFixablesComponent

                        }
                        <Text style={{ fontSize: 16, marginBottom: 10, marginTop: 10 }}>
                            {I18n.t('results.security')}
                        </Text>
                        {securityFixables.length > 0 ?
                            securityFixables.map(fixable => (
                                <View key={fixable.checkItemId}
                                    style={styles.fixableRow}>
                                    <Text style={styles.fixableRowText}>{fixable.text}</Text>
                                    <TouchableOpacity
                                        style={styles.fixableRowButton}
                                        onPress={() => this.navigateToCheckItem('Security', fixable.target, fixable.checkItemId)}>
                                        <Icon
                                            name="arrow-right"
                                            color={Theme.gray}
                                            size={30} />
                                    </TouchableOpacity>
                                </View>)) : noFixablesComponent

                        }
                        <Text style={{ fontSize: 16, marginBottom: 10, marginTop: 10 }}>
                            {I18n.t('results.securityWalk')}
                        </Text>
                        {securityWalkFixables.length > 0 ?
                            securityWalkFixables.map(fixable => (
                                <View key={fixable.checkItemId}
                                    style={styles.fixableRow}>
                                    <Text style={styles.fixableRowText}>{fixable.text}</Text>
                                    <TouchableOpacity
                                        style={styles.fixableRowButton}
                                        onPress={() => this.navigateToCheckItem('SecurityWalk', fixable.target, fixable.checkItemId)}>
                                        <Icon
                                            name="arrow-right"
                                            color={Theme.gray}
                                            size={30} />
                                    </TouchableOpacity>
                                </View>)) : noFixablesComponent

                        }
                        <Text style={{ fontSize: 16, marginBottom: 10, marginTop: 10 }}>
                            {I18n.t('results.workplaceSecurityCheck')}
                        </Text>
                        {workplaceSecurityCheckFixables.length > 0 ?
                            workplaceSecurityCheckFixables.map(fixable => (
                                <View key={fixable.checkItemId}
                                    style={styles.fixableRow}>
                                    <Text style={styles.fixableRowText}>{fixable.text}</Text>
                                    <TouchableOpacity
                                        style={styles.fixableRowButton}
                                        onPress={() => this.navigateToCheckItem('WorkplaceSecurityCheck', fixable.target, fixable.checkItemId)}>
                                        <Icon
                                            name="arrow-right"
                                            color={Theme.gray}
                                            size={30} />
                                    </TouchableOpacity>
                                </View>)) : noFixablesComponent

                        }
                    </View>
					<View style={{ paddingTop: Theme.screenMargin }}>
						<IconButton
							color={Theme.orange}
							text={I18n.t('common.sendPrivateResults')}
							disabled={false}
							icon="envelope"
							onPress={() => this.props.sendPrivateResults()} />
					</View>
					<View style={{ paddingTop: Theme.screenMargin }}>
						<IconButton
							color={Theme.orange}
							text={I18n.t('common.sendPublicResults')}
							disabled={false}
							icon="envelope"
							onPress={() => this.props.sendPublicResults()} />
					</View>
                </ScrollView>
            </View>
        );
    }
}

const getThingsToFix = (checklist: any, answers: any, targets: Target[]) => {
    const thingsToFix: Fixable[] = _.chain(answers)
        .filter((a: any) => a.answer === SafetyCheckAnswerType.NotOk)
        .map(a => ({
            targetId: a.targetId,
            checkItem: _.find(checklist, (i: any) => i.id === a.checkItemId)
        }))
        .filter(item => item.checkItem != null)
        .map(item => ({
            target: targets.find(t => t.id === item.targetId)!!,
            checkItemId: item.checkItem!!.id,
            text: item.checkItem!!.text
        }))
        .filter(item => item.target != null)
        .value();
    return thingsToFix;
}

const mapStateToProps = (state: AppState) => {
    return {
        targets: state.targets,
        selectedTarget: state.selectedTarget,
        safetyCheckItems: state.safetyChecklist,
        securityCheckItems: state.securityChecklist, /* modified from safetyChecklist */
        securityWalkCheckItems: state.securityWalkChecklist,
        workplaceSecurityCheckItems: state.workplaceSecurityChecklist,
        safetyThingsToFix: getThingsToFix(state.safetyChecklist, state.safetyCheckAnswers, state.targets),
        securityThingsToFix: getThingsToFix(state.securityChecklist, state.securityCheckAnswers, state.targets),
        securityWalkThingsToFix: getThingsToFix(state.securityWalkChecklist, state.securityWalkCheckAnswers, state.targets),
        workplaceSecurityCheckThingsToFix: getThingsToFix(state.workplaceSecurityChecklist, state.workplaceSecurityCheckAnswers, state.targets)
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        sendPrivateResults: () => {
            const action: SendPrivateResultsAction = {
                type: ActionTypes.SendPrivateResults
            }
            dispatch(action);
        },
        sendPublicResults: () => {
            const action: SendPublicResultsAction = {
                type: ActionTypes.SendPublicResults
            }
            dispatch(action);
        },
        selectTarget: (target: Target) => {
            const action: SelectTargetAction = {
                type: ActionTypes.SelectTarget,
                target
            };
            dispatch(action);
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ResultsScreen)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Theme.white,
        padding: Theme.screenMargin
    },
    resultsContainer: {
        alignSelf: 'stretch',
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 10,
        marginTop: 10
    },
    noFixables: {
        flex: 1,
        flexDirection: 'row',
        paddingLeft: 20,
        alignItems: 'center'
    },
    targetRow: {
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: Theme.gray,
        padding: 5,
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: Theme.gray,
        borderRadius: 5,
        marginTop: 3,
        marginBottom: 3,
    },
    selectedTargetRow: {
        backgroundColor: Theme.gray
    },
    fixableRow: {
        flex: 1,
        alignSelf: 'stretch',
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 20,
        paddingLeft: 20
    },
    fixableRowText: {
        flex: 1,
        paddingRight: 10
    },
    fixableRowButton: {
        paddingRight: 10
    }
});
