import * as React from "react";
import { View, Image, StyleSheet, Text, ScrollView, TouchableOpacity } from "react-native";
import { NavigationScreenProps } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Theme } from '../theme';
import { connect } from 'react-redux'
import I18n from 'react-native-i18n';
import { AppState, SupportedLocale, Target } from '../AppState';
import { SelectTargetAction, ActionTypes } from "../Actions";


interface Props extends NavigationScreenProps<{}> {
    locale: SupportedLocale;
    targets: Target[];
    selectTarget: (target: Target) => void;
}

interface State {

}

class HomeScreen extends React.Component<Props, State> {

    navigateToSafety() {
        const { navigate } = this.props.navigation;
        // If there's only one target we can skip target selection
        if (this.props.targets.length === 1) {
            this.props.selectTarget(this.props.targets[0]);
            navigate('Safety', {
                title: this.props.targets[0].name,
                showList: false,
                hasOneTarget: true,
                checkIndex: 0
            })
        } else {
            navigate('TargetSelection', { title: I18n.t('safety.title'), target: 'Safety' })
        }
    }

    navigateToSecurity() {
        const { navigate } = this.props.navigation;
        // If there's only one target we can skip target selection
        if (this.props.targets.length === 1) {
            this.props.selectTarget(this.props.targets[0]);
            navigate('Security', {
                title: this.props.targets[0].name,
                showList: false,
                hasOneTarget: true,
                checkIndex: 0
            })
        } else {
            navigate('TargetSelection', { title: I18n.t('security.title'), target: 'Security' })
        }
    }

    navigateToSecurityWalk() {
        const { navigate } = this.props.navigation;
        // If there's only one target we can skip target selection
        if (this.props.targets.length === 1) {
            this.props.selectTarget(this.props.targets[0]);
            navigate('SecurityWalk', {
                title: this.props.targets[0].name,
                showList: false,
                hasOneTarget: true,
                checkIndex: 0
            })
        } else {
            navigate('TargetSelection', { title: I18n.t('securityWalk.title'), target: 'SecurityWalk' })
        }
    }

    navigateToWorkplaceSecurityCheck() {
        const { navigate } = this.props.navigation;
        // If there's only one target we can skip target selection
        if (this.props.targets.length === 1) {
            this.props.selectTarget(this.props.targets[0]);
            navigate('WorkplaceSecurityCheck', {
                title: this.props.targets[0].name,
                showList: false,
                hasOneTarget: true,
                checkIndex: 0
            })
        } else {
            navigate('TargetSelection', { title: I18n.t('workplaceSecurityCheck.title'), target: 'WorkplaceSecurityCheck' })
        }
    }

    render() {
        I18n.locale = this.props.locale;
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image
                        source={require('../../images/turva_pro_logo.png')} />
                </View>
                <ScrollView contentContainerStyle={styles.main}>
                    <TouchableOpacity onPress={() => this.navigateToSafety()}>
                        <Text style={styles.mainButton}>
                            {I18n.t('home.nav.safety').toUpperCase()}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.navigateToSecurity()}>
                        <Text style={styles.mainButton}>
                            {I18n.t('home.nav.security').toUpperCase()}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.navigateToSecurityWalk()}>
                        <Text style={styles.mainButton}>
                            {I18n.t('home.nav.securityWalk').toUpperCase()}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.navigateToWorkplaceSecurityCheck()}>
                        <Text style={styles.mainButton}>
                            {I18n.t('home.nav.workplaceSecurityCheck').toUpperCase()}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigate('Results', { title: I18n.t('results.title') })}>
                        <Text style={styles.resultsButton}>
                            <Icon name="file-text-o" size={20} /> {I18n.t('home.nav.results')}
                        </Text>
                    </TouchableOpacity>
                    <View style={styles.footer}>
                        <TouchableOpacity onPress={() => navigate('Settings', { title: I18n.t('settings.title') })}>
                            <Icon
                                name="cog"
                                color={Theme.gray}
                                size={38} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigate('AppInfo', { title: I18n.t('info.title') })}>
                            <Icon
                                name="info-circle"
                                color={Theme.gray}
                                size={38} />
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        selectTarget: (target: Target) => {
            const action: SelectTargetAction = {
                type: ActionTypes.SelectTarget,
                target
            };
            dispatch(action);
        }
    }
}

export default connect((state: AppState) => ({
    locale: state.currentLocale,
    targets: state.targets
}), mapDispatchToProps)(HomeScreen);

const margin = 5;
const button = {
    padding: 20,
    fontSize: 20
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Theme.lightGray,
    },
    header: {
        height: 100,
        alignSelf: 'stretch',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Theme.white,
    },
    main: {
        alignSelf: 'stretch',
        justifyContent: 'center',
        alignContent: 'center',
        padding: margin,
        backgroundColor: Theme.lightGray,
    },
    mainButton: {
        alignSelf: 'stretch',
        padding: button.padding,
        fontSize: button.fontSize,
        borderRadius: 5,
        margin: margin,
        backgroundColor: Theme.black,
        color: Theme.white,
        textAlign: 'center',
    },
    resultsButton: {
        alignSelf: 'stretch',
        padding: button.padding,
        fontSize: button.fontSize,
        borderRadius: 5,
        margin: margin,
        backgroundColor: Theme.orange,
        color: Theme.white,
        textAlign: 'center',
    },
    footer: {
        flex: 1,
        alignSelf: 'stretch',
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: margin,
        backgroundColor: Theme.lightGray,
    }
});
