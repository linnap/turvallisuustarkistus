import * as React from "react";
import { TouchableOpacity, Text, StyleSheet } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
import { Theme } from '../theme';

interface Props {
    disabled: boolean;
    onPress: () => void;
    text: string;
    icon: string;
    color: string;
}

export const IconButton = (props: Props) => {
    const buttonStyle = StyleSheet.flatten([styles.button, { backgroundColor: props.color }])
    return (
        <TouchableOpacity
            disabled={props.disabled}
            onPress={() => props.onPress()}>
            <Text style={props.disabled ? styles.disabledButton : buttonStyle}>
                <Icon name={props.icon} size={20} /> {props.text}
            </Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    button: {
        alignSelf: 'stretch',
        padding: 20,
        fontSize: 20,
        borderRadius: 5,
        backgroundColor: Theme.gray,
        color: Theme.white,
        textAlign: 'center',
    },
    disabledButton: {
        alignSelf: 'stretch',
        padding: 20,
        fontSize: 20,
        borderRadius: 5,
        backgroundColor: Theme.gray,
        color: '#777',
        textAlign: 'center',
    }
});
