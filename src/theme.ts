export const Theme = {
    white: '#FFFFFF',
    red: '#D0081C',
    orange: '#EC7404',
    lightGray: '#DCCFC0',
    black: '#000000',
    gray: '#333333',
    screenMargin: 10,
}
