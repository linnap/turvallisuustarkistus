import * as React from "react";
import { View, StyleSheet, Text, TextInput, ScrollView } from "react-native";
import { NavigationScreenProps } from 'react-navigation';
import { Theme } from '../theme';
import { connect } from 'react-redux'
import { AppState } from "../AppState";
import I18n from 'react-native-i18n';
import { RemoveTargetAction, ActionTypes, UpdateTargetAction, ClearResultsAction } from '../Actions';
import { find } from 'lodash';
import { IconButton } from '../Components/IconButton';

interface Props extends NavigationScreenProps<{ targetId: number, name: string }> {
    targetName: string;
    targetAddress: string;
    targetCity: string;
    removeTarget: (targetId: number) => void;
    update: (targetId: number, name: string, address: string, city: string) => void;
    clearResults: (targetId: number) => void;
}

interface State {
    name: string,
    address: string,
    city: string
}

class TargetDetailsScreen extends React.Component<Props, State> {

    static navigationOptions = ({ navigation, screenProps }) => {
        return { title: navigation.state.params.name }
    };

    constructor(props: Props) {
        super(props);
        this.state = {
            name: props.targetName,
            address: props.targetAddress,
            city: props.targetCity
        }
    }

    updateTargetAndNavigateBack() {
        this.props.update(this.props.navigation.state.params.targetId, this.state.name, this.state.address, this.state.city);
        this.props.navigation.goBack();
    }

    clearResultsAndNavigateBack() {
        this.props.clearResults(this.props.navigation.state.params.targetId);
        this.props.navigation.goBack();
    }

    removeTargetAndNavigateBack() {
        this.props.removeTarget(this.props.navigation.state.params.targetId);
        this.props.navigation.goBack();
    }

    render() {
        const saveDisabled = this.state.name == null || this.state.name.length === 0;
        return (
            <View style={styles.pageContainer}>
                <ScrollView
                    contentContainerStyle={styles.container}
                    keyboardShouldPersistTaps="handled">
                    <Text>{I18n.t('target.nameFieldTitle')}</Text>
                    <TextInput
                        style={styles.field}
                        value={this.state.name}
                        returnKeyType="next"
                        onSubmitEditing={() => { (this.refs.AddressInput as any).focus(); }}
                        onChangeText={name => this.setState({ name })} />
                    <Text>{I18n.t('target.addressFieldTitle')}</Text>
                    <TextInput
                        ref="AddressInput"
                        style={styles.field}
                        value={this.state.address}
                        returnKeyType="next"
                        onSubmitEditing={() => { (this.refs.CityInput as any).focus(); }}
                        onChangeText={address => this.setState({ address })} />
                    <Text>{I18n.t('target.cityFieldTitle')}</Text>
                    <TextInput
                        ref="CityInput"
                        style={styles.field}
                        value={this.state.city}
                        returnKeyType="done"
                        onChangeText={city => this.setState({ city })} />
                    <IconButton
                        color={Theme.gray}
                        disabled={saveDisabled}
                        icon="archive"
                        text={I18n.t('target.saveName')}
                        onPress={() => this.updateTargetAndNavigateBack()}
                    />
                    <View style={styles.divider} />
                    <IconButton
                        color={Theme.gray}
                        disabled={false}
                        icon="file-o"
                        text={I18n.t('target.clearResults')}
                        onPress={() => this.clearResultsAndNavigateBack()}
                    />
                    <View style={styles.divider} />
                    <IconButton
                        color={Theme.red}
                        disabled={false}
                        icon="trash"
                        text={I18n.t('target.remove')}
                        onPress={() => this.removeTargetAndNavigateBack()}
                    />
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = (state: AppState, ownProps: Props) => {
    const target = find(state.targets, t => t.id === ownProps.navigation.state.params.targetId);
    return {
        targetName: target ? target.name : '',
        targetAddress: target ? target.address : '',
        targetCity: target ? target.city : ''
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        removeTarget: (targetId: number) => {
            const action: RemoveTargetAction = {
                type: ActionTypes.RemoveTarget,
                targetId
            }
            dispatch(action)
        },
        update: (targetId: number, name: string, address: string, city: string) => {
            const action: UpdateTargetAction = {
                type: ActionTypes.UpdateTarget,
                targetId,
                name, address, city
            };
            dispatch(action);
        },
        clearResults: (targetId: number) => {
            const action: ClearResultsAction = {
                type: ActionTypes.ClearResults,
                targetId
            };
            dispatch(action);
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TargetDetailsScreen);

const styles = StyleSheet.create({
    pageContainer: {
        flex: 1,
        backgroundColor: Theme.lightGray
    },
    container: {
        padding: 20,
        alignSelf: 'stretch',
        backgroundColor: Theme.lightGray,
    },
    divider: {
        marginBottom: 20,
    },
    field: {
        flex: 0,
        fontSize: 20,
        marginBottom: 5
    }
});
