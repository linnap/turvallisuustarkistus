import * as React from "react";
import { View, Image, StyleSheet, ScrollView, Text } from "react-native";
import { NavigationScreenProps } from 'react-navigation';
import { Theme } from '../theme';
import I18n from 'react-native-i18n';

interface State {

}

export class AppInfoScreen extends React.Component<NavigationScreenProps<{}>, State> {

    static navigationOptions = ({ navigation, screenProps }) => {
        return ({
            title: navigation.state.params.title
        });
    };

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: Theme.white, paddingTop: 20 }}>
                <ScrollView contentContainerStyle={styles.container}>
                    <Text style={{ padding: Theme.screenMargin }}>
                        {I18n.t('info.descriptionFirstParagraph')}
                    </Text>
                    <Text style={{ padding: Theme.screenMargin }}>
                        {I18n.t('info.descriptionSecondParagraph')}
                    </Text>
                    <Image style={styles.logo} source={require('../../images/tty.png')} />
                    <Image style={styles.logo} source={require('../../images/satakuntaliitto.png')} />
                    <Image style={styles.logo} source={require('../../images/vipuvoimaa.png')} />
                    <Image style={styles.logo} source={require('../../images/aluekehitysrahasto.png')} />
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Theme.white,
    },
    logo: {
        marginBottom: 20
    }
});
