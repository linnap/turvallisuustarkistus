interface I18n {
    defaultLocalt: string;
    locale: string;
    translations: { [language: string]: any };
    currentLocale: () => string;
    t: (key: string, options?: TranslateOptions) => string;
}

interface TranslateOptions {
    defaultValue: string;
}

declare module 'react-native-i18n' {
    export default <I18n>{};
}
