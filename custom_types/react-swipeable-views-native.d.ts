import SwipeableViews from 'react-swipeable-views';

declare module 'react-swipeable-views-native' {
    export default SwipeableViews;
}
